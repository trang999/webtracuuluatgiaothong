﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebTracuuLuatGiaoThong.Models;

#nullable disable

namespace WebTracuuLuatGiaoThong.Migrations
{
    [DbContext(typeof(DbDacnContext))]
    [Migration("20241011150851_db")]
    partial class db
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "8.0.4")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("AspNetRoleAspNetUser", b =>
                {
                    b.Property<string>("RolesId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("UsersId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("RolesId", "UsersId");

                    b.HasIndex("UsersId");

                    b.ToTable("AspNetRoleAspNetUser");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("NormalizedName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("AspNetRole");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetRoleClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaim");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("Address")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ConcurrencyStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("NormalizedUserName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("AspNetUser");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUserClaim", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaim");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUserLogin", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex(new[] { "UserId" }, "IX_AspNetUserLogins_UserId");

                    b.ToTable("AspNetUserLogin");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUserToken", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserToken");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.BinhLuan", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int?>("IdPhanhoi")
                        .HasColumnType("int");

                    b.Property<int>("IdTintuc")
                        .HasColumnType("int");

                    b.Property<string>("IdUser")
                        .IsRequired()
                        .HasMaxLength(450)
                        .HasColumnType("nvarchar(450)");

                    b.Property<DateTime>("Ngaybinhluan")
                        .HasColumnType("datetime");

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TrangThai")
                        .HasColumnType("bit");

                    b.HasKey("Id");

                    b.HasIndex("IdTintuc");

                    b.HasIndex("IdUser");

                    b.ToTable("BinhLuan", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Chuong", b =>
                {
                    b.Property<int>("Idchuong")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDchuong");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Idchuong"));

                    b.Property<int>("Idvb")
                        .HasColumnType("int")
                        .HasColumnName("IDvb");

                    b.Property<string>("Ten")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Idchuong");

                    b.HasIndex(new[] { "Idvb" }, "IX_Chuong_IDvb");

                    b.ToTable("Chuong", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuKhoan", b =>
                {
                    b.Property<int>("Iddk")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDdk");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Iddk"));

                    b.Property<int>("Iddl")
                        .HasColumnType("int")
                        .HasColumnName("IDdl");

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Iddk");

                    b.HasIndex(new[] { "Iddl" }, "IX_DieuKhoan_IDdl");

                    b.ToTable("DieuKhoan", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuKhoanThanhPhan", b =>
                {
                    b.Property<int>("Iddktp")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDdktp");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Iddktp"));

                    b.Property<int>("Iddk")
                        .HasColumnType("int")
                        .HasColumnName("IDdk");

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Iddktp");

                    b.HasIndex(new[] { "Iddk" }, "IX_DieuKhoanThanhPhan_IDdk");

                    b.ToTable("DieuKhoanThanhPhan", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuLuat", b =>
                {
                    b.Property<int>("Iddl")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDdl");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Iddl"));

                    b.Property<int>("Idchuong")
                        .HasColumnType("int")
                        .HasColumnName("IDchuong");

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Iddl");

                    b.HasIndex(new[] { "Idchuong" }, "IX_DieuLuat_IDchuong");

                    b.ToTable("DieuLuat", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Ghichu", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("ID");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<int?>("Iddk")
                        .HasColumnType("int")
                        .HasColumnName("IDdk");

                    b.Property<int?>("Iddktp")
                        .HasColumnType("int")
                        .HasColumnName("IDdktp");

                    b.Property<int?>("Iddl")
                        .HasColumnType("int")
                        .HasColumnName("IDdl");

                    b.Property<string>("Iduser")
                        .IsRequired()
                        .HasMaxLength(450)
                        .HasColumnType("nvarchar(450)")
                        .HasColumnName("IDUser");

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("Iduser");

                    b.HasIndex(new[] { "Iddk" }, "IX_Ghichu_IDdk");

                    b.HasIndex(new[] { "Iddktp" }, "IX_Ghichu_IDdktp");

                    b.HasIndex(new[] { "Iddl" }, "IX_Ghichu_IDdl");

                    b.ToTable("Ghichu", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.LoaiVanBan", b =>
                {
                    b.Property<int>("Idloaivb")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDLoaivb");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Idloaivb"));

                    b.Property<string>("TenLoaivb")
                        .IsRequired()
                        .HasMaxLength(200)
                        .HasColumnType("nvarchar(200)");

                    b.HasKey("Idloaivb");

                    b.ToTable("LoaiVanBan", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Tintuc", b =>
                {
                    b.Property<int>("Idtintuc")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDtintuc");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Idtintuc"));

                    b.Property<string>("Iduser")
                        .IsRequired()
                        .HasMaxLength(450)
                        .HasColumnType("nvarchar(450)")
                        .HasColumnName("IDUser");

                    b.Property<DateTime>("Ngaycapnhat")
                        .HasColumnType("datetime");

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Ten")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("Trangthai")
                        .HasColumnType("bit");

                    b.HasKey("Idtintuc");

                    b.HasIndex("Iduser");

                    b.ToTable("Tintuc", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.TuCam", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("Noidung")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.HasKey("Id");

                    b.ToTable("TuCam", (string)null);
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Vanban", b =>
                {
                    b.Property<int>("Idvb")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("IDvb");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Idvb"));

                    b.Property<string>("ChucvuNguoikibanhanh")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("nvarchar(max)")
                        .HasDefaultValue("");

                    b.Property<string>("Coquanphathanh")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("IdloaiVanBan")
                        .HasColumnType("int")
                        .HasColumnName("IDLoaiVanBan");

                    b.Property<DateOnly>("Ngaybanhanh")
                        .HasColumnType("date");

                    b.Property<DateOnly>("Ngaycapnhat")
                        .HasColumnType("date");

                    b.Property<string>("Nguoikibanhanh")
                        .IsRequired()
                        .ValueGeneratedOnAdd()
                        .HasColumnType("nvarchar(max)")
                        .HasDefaultValue("");

                    b.Property<string>("Noibanhanh")
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("Sohieuvanban")
                        .IsRequired()
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("Ten")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Idvb");

                    b.HasIndex(new[] { "IdloaiVanBan" }, "IX_Vanban_IDLoaiVanBan");

                    b.ToTable("Vanban", (string)null);
                });

            modelBuilder.Entity("AspNetRoleAspNetUser", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetRole", null)
                        .WithMany()
                        .HasForeignKey("RolesId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", null)
                        .WithMany()
                        .HasForeignKey("UsersId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("WebTracuuLuatGiaoThong.Models.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.ApplicationUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetRoleClaim", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetRole", "Role")
                        .WithMany("AspNetRoleClaims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Role");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUserClaim", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", "User")
                        .WithMany("AspNetUserClaims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUserLogin", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", "User")
                        .WithMany("AspNetUserLogins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUserToken", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", "User")
                        .WithMany("AspNetUserTokens")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("User");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.BinhLuan", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.Tintuc", "IdTintucNavigation")
                        .WithMany("BinhLuans")
                        .HasForeignKey("IdTintuc")
                        .IsRequired()
                        .HasConstraintName("FK_BinhLuan_Tintuc1");

                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", "IdUserNavigation")
                        .WithMany("BinhLuans")
                        .HasForeignKey("IdUser")
                        .IsRequired()
                        .HasConstraintName("FK_BinhLuan_AspNetUsers");

                    b.Navigation("IdTintucNavigation");

                    b.Navigation("IdUserNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Chuong", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.Vanban", "IdvbNavigation")
                        .WithMany("Chuongs")
                        .HasForeignKey("Idvb")
                        .IsRequired()
                        .HasConstraintName("FK_Chuong_Vanban");

                    b.Navigation("IdvbNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuKhoan", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.DieuLuat", "IddlNavigation")
                        .WithMany("DieuKhoans")
                        .HasForeignKey("Iddl")
                        .IsRequired()
                        .HasConstraintName("FK_DieuKhoan_DieuLuat");

                    b.Navigation("IddlNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuKhoanThanhPhan", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.DieuKhoan", "IddkNavigation")
                        .WithMany("DieuKhoanThanhPhans")
                        .HasForeignKey("Iddk")
                        .IsRequired()
                        .HasConstraintName("FK_DieuKhoanThanhPhan_DieuKhoan");

                    b.Navigation("IddkNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuLuat", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.Chuong", "IdchuongNavigation")
                        .WithMany("DieuLuats")
                        .HasForeignKey("Idchuong")
                        .IsRequired()
                        .HasConstraintName("FK_DieuLuat_Chuong");

                    b.Navigation("IdchuongNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Ghichu", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.DieuKhoan", "IddkNavigation")
                        .WithMany("Ghichus")
                        .HasForeignKey("Iddk")
                        .HasConstraintName("FK_Ghichu_DieuKhoan");

                    b.HasOne("WebTracuuLuatGiaoThong.Models.DieuKhoanThanhPhan", "IddktpNavigation")
                        .WithMany("Ghichus")
                        .HasForeignKey("Iddktp")
                        .HasConstraintName("FK_Ghichu_DieuKhoanThanhPhan");

                    b.HasOne("WebTracuuLuatGiaoThong.Models.DieuLuat", "IddlNavigation")
                        .WithMany("Ghichus")
                        .HasForeignKey("Iddl")
                        .HasConstraintName("FK_Ghichu_DieuLuat");

                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", "IduserNavigation")
                        .WithMany("Ghichus")
                        .HasForeignKey("Iduser")
                        .IsRequired()
                        .HasConstraintName("FK_Ghichu_AspNetUsers");

                    b.Navigation("IddkNavigation");

                    b.Navigation("IddktpNavigation");

                    b.Navigation("IddlNavigation");

                    b.Navigation("IduserNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Tintuc", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.AspNetUser", "IduserNavigation")
                        .WithMany("Tintucs")
                        .HasForeignKey("Iduser")
                        .IsRequired()
                        .HasConstraintName("FK_Tintuc_AspNetUsers");

                    b.Navigation("IduserNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Vanban", b =>
                {
                    b.HasOne("WebTracuuLuatGiaoThong.Models.LoaiVanBan", "IdloaiVanBanNavigation")
                        .WithMany("Vanbans")
                        .HasForeignKey("IdloaiVanBan")
                        .IsRequired()
                        .HasConstraintName("FK_Vanban_LoaiVanBan");

                    b.Navigation("IdloaiVanBanNavigation");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetRole", b =>
                {
                    b.Navigation("AspNetRoleClaims");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.AspNetUser", b =>
                {
                    b.Navigation("AspNetUserClaims");

                    b.Navigation("AspNetUserLogins");

                    b.Navigation("AspNetUserTokens");

                    b.Navigation("BinhLuans");

                    b.Navigation("Ghichus");

                    b.Navigation("Tintucs");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Chuong", b =>
                {
                    b.Navigation("DieuLuats");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuKhoan", b =>
                {
                    b.Navigation("DieuKhoanThanhPhans");

                    b.Navigation("Ghichus");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuKhoanThanhPhan", b =>
                {
                    b.Navigation("Ghichus");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.DieuLuat", b =>
                {
                    b.Navigation("DieuKhoans");

                    b.Navigation("Ghichus");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.LoaiVanBan", b =>
                {
                    b.Navigation("Vanbans");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Tintuc", b =>
                {
                    b.Navigation("BinhLuans");
                });

            modelBuilder.Entity("WebTracuuLuatGiaoThong.Models.Vanban", b =>
                {
                    b.Navigation("Chuongs");
                });
#pragma warning restore 612, 618
        }
    }
}
