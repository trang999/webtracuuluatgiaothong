﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using WebTracuuLuatGiaoThong.Models;
using WebTracuuLuatGiaoThong.Repository.Admin;
using NAudio.Wave;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;

namespace WebTracuuLuatGiaoThong.Controllers.Admin
{
    public class AdminController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IAdminRepository _adminRepository;

        private readonly BlobServiceClient _blobServiceClient;

        public AdminController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IAdminRepository adminRepository, BlobServiceClient blobServiceClient)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _adminRepository = adminRepository;
            _blobServiceClient = blobServiceClient;
        }
        public async Task<IActionResult> TrangchuAdmin()
        {
            var users = await _userManager.Users.CountAsync();
            var vanban = await _adminRepository.GetallVanban();
            int SLvb = vanban.Count();
            var tintuc = await _adminRepository.GetAllTintuc();
            int SLtt = tintuc.Count();
            var ListTintuc = await _adminRepository.TintucKiemduyet();
            int kiemduyet = ListTintuc.Count();

            ViewBag.UserCount = users;
            ViewBag.VanbanCount = SLvb;
            ViewBag.TintucCount = SLtt;
            ViewBag.KiemduyetCount = kiemduyet;

            return View();
        }

        public async Task<IActionResult> ListUser()
        {
            var users = await _userManager.Users.ToListAsync();

            var usersWithRoles = new List<object>();

            foreach (var user in users)
            {
                var roles = await _userManager.GetRolesAsync(user);

                var userWithRoles = new
                {
                    UserId = user.Id,
                    UserName = user.UserName,
                    Address = user.Address,
                    RoleNames = roles
                };

                usersWithRoles.Add(userWithRoles);
            }
            return View(usersWithRoles);
        }

        public async Task<IActionResult> ListUserBan()
        {
            var binhluanList = await _adminRepository.BinhluanKiemduyet();//danh sách bình luận có trạng thái = false
            if (binhluanList.Count == 0)
            {
                return View();
            }
            var ListBanUserID = binhluanList
                .GroupBy(b => b.IdUser)
                .Where(g => g.Count() >= 3)
                .Select(g => g.Key)
                .ToList();
            if (ListBanUserID.Count > 0)
            {
                var usersBan = await _userManager.Users.Where(u => ListBanUserID.Contains(u.Id)).ToListAsync();
                ViewBag.ListUserBan = usersBan;
            }
            return View();  
        }


        [HttpPost]
        public async Task<IActionResult> UpdateUserRole(string userId, string userType)
        {
            var user = await _userManager.FindByIdAsync(userId);

            if (user == null)
            {
                return NotFound();
            }
            var currentRoles = await _userManager.GetRolesAsync(user);
            await _userManager.RemoveFromRolesAsync(user, currentRoles);
            await _userManager.AddToRoleAsync(user, userType);
            return RedirectToAction("ListUser", "Admin");
        }
        [HttpPost]
        public async Task<IActionResult> ConfirmRemoveUser(string Userid)
        {
            var user = await _userManager.FindByIdAsync(Userid);
            if (user == null) return NotFound("Không tìm thấy người dùng trong cơ sở dữ liệu");
            ViewBag.UserName = user.UserName;
            ViewBag.UserID = user.Id;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> RemoveUser(string Userid)
        {
            var user = await _userManager.FindByIdAsync(Userid);
            if (user == null) return NotFound("Không tìm thấy người dùng trong cơ sở dữ liệu");
            await _adminRepository.DeleteUserConnect(Userid);
            await _userManager.DeleteAsync(user);
            TempData["Message"] = "Xóa người dùng thành công!";
            TempData["MessageType"] = "success";
            return RedirectToAction("ListUser", "Admin");
        }
        [HttpPost]
        public async Task<IActionResult> UpdateRole(string noidung, int loai, int id, int idVanban)
        {
            if (loai == 1)
            {
                var chuong = await _adminRepository.timkiemchuong(id);
                if (chuong == null)

                    return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                else
                {
                    chuong.Ten = noidung;
                    bool ktra = await _adminRepository.ChinhsuaChuong(chuong);
                    if (ktra == false)
                    {
                        TempData["Message"] = "Thao tác chỉnh sửa không thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                    else
                    {
                        TempData["Message"] = "Chỉnh sửa chương thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                }
            }
            if (loai == 2)
            {
                var dieuluat = await _adminRepository.timkiemdieuluat(id);
                if (dieuluat == null)
                    return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                else
                {
                    dieuluat.Noidung = noidung;
                    bool ktra = await _adminRepository.ChinhsuaDieuluat(dieuluat);
                    if (ktra == false)
                    {
                        TempData["Message"] = "Thao tác chỉnh sửa không thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                    else
                    {
                        TempData["Message"] = "Chỉnh sửa điều luật thành công dùng thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                }
            }
            if (loai == 3)
            {
                var dieuKhoan = await _adminRepository.timkiemdieukhoan(id);
                if (dieuKhoan == null)
                    return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                else
                {
                    dieuKhoan.Noidung = noidung;
                    bool ktra = await _adminRepository.ChinhsuaDieukhoan(dieuKhoan);
                    if (ktra == false)
                    {
                        TempData["Message"] = "Thao tác chỉnh sửa không thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                    else
                    {
                        TempData["Message"] = "Chỉnh sửa điều khoản thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                }
            }
            if (loai == 4)
            {
                var dieuKhoantp = await _adminRepository.timkiemdieukhoanthanhphan(id);
                if (dieuKhoantp == null)
                    return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                else
                {
                    dieuKhoantp.Noidung = noidung;
                    bool ktra = await _adminRepository.ChinhsuaDieukhoanthanhphan(dieuKhoantp);
                    if (ktra == false)
                    {
                        TempData["Message"] = "Thao tác chỉnh sửa không thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                    else
                    {
                        TempData["Message"] = "Chỉnh sửa điều khoản thành phần thành công!";
                        TempData["MessageType"] = "success";
                        return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
                    }
                }
            }
            return RedirectToAction("Suavanban", "Admin", new { id = idVanban });
        }

        public async Task<IActionResult> AddNewVanBan()
        {
            var listloai = await _adminRepository.ListLoaiVanban();
            ViewBag.ListLoaiVanban = new SelectList(listloai, "Idloaivb", "TenLoaivb");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddVanban(Vanban vanban)////////////////////////////////////////////////////////
        {
            string ngayBanHanhString = vanban.Ngaybanhanh.ToString("yyyy/dd/MM");
            string ngayCapNhatString = vanban.Ngaycapnhat.ToString("yyyy/dd/MM");

            DateTime ngayBanHanh = DateTime.ParseExact(ngayBanHanhString, "yyyy/dd/MM", CultureInfo.InvariantCulture);
            DateTime ngayCapNhat = DateTime.ParseExact(ngayCapNhatString, "yyyy/dd/MM", CultureInfo.InvariantCulture);

            vanban.Ngaybanhanh = new DateOnly(ngayBanHanh.Year, ngayBanHanh.Month, ngayBanHanh.Day);
            vanban.Ngaycapnhat = new DateOnly(ngayCapNhat.Year, ngayCapNhat.Month, ngayCapNhat.Day);

            bool ktra = await _adminRepository.ThemVanban(vanban);
            if (ktra == false)
            {
                return RedirectToAction("AddNewVanBan", "Admin");
            }
            int id = await _adminRepository.GetIDnewVanban();
            return RedirectToAction("Luatgiaothongchitiet", "Admin", new { id = id });
        }

        public async Task<IActionResult> Luatgiaothongchitiet(int id)
        {
            var noidung = await _adminRepository.GetNoidungcuaVanban(id);
            return View(noidung);
        }
        [HttpPost]
        public async Task<IActionResult> AddBosung(string content,int Idloai, int idvb)//controller add bo sung cho van ban
        {
            BosungVanban bosungVanban = new BosungVanban();
            bosungVanban.Noidung = content;
            bosungVanban.Idvb = idvb;
            bosungVanban.IdloaiBs = Idloai;

            await _adminRepository.ThemBosung(bosungVanban);
            return RedirectToAction("Luatgiaothongchitiet", new { id = idvb });
        }

        public async Task<IActionResult> Suavanban(int id)
        {
            var noidung = await _adminRepository.GetNoidungcuaVanban(id);
            return View(noidung);
        }
        List<Chuong> Listchuong = new List<Chuong>();
        List<DieuLuat> ListDL = new List<DieuLuat>();
        List<DieuKhoan> ListDK = new List<DieuKhoan>();
        List<DieuKhoanThanhPhan> ListDKTP = new List<DieuKhoanThanhPhan>();
        [HttpPost]
        public async Task<IActionResult> AddChuong(string content, int idvb)
        {
            Chuong chuong = new Chuong();
            chuong.Ten = content;
            chuong.Idvb = idvb;
            await _adminRepository.ThemChuong(chuong);
            return RedirectToAction("Luatgiaothongchitiet", new { id = idvb });
        }
        [HttpPost]
        public async Task<IActionResult> AddDieuluat(int idvb, string content, int idchuong)
        {
            DieuLuat dl = new DieuLuat();
            dl.Idchuong = idchuong;
            dl.Noidung = content;
            await _adminRepository.ThemDieuLuat(dl);
            return RedirectToAction("Luatgiaothongchitiet", new { id = idvb });
        }
        [HttpPost]
        public async Task<IActionResult> AddDieukhoan(int idvb, string content, int iddl)
        {
            DieuKhoan dk = new DieuKhoan();
            dk.Iddl = iddl;
            dk.Noidung = content;
            await _adminRepository.ThemDieuKhoan(dk);
            return RedirectToAction("Luatgiaothongchitiet", new { id = idvb });
        }
        [HttpPost]
        public async Task<IActionResult> AddDieukhoanthanhphan(int idvb, string content, int iddk)
        {
            DieuKhoanThanhPhan dktp = new DieuKhoanThanhPhan();
            dktp.Noidung = content;
            dktp.Iddk = iddk;
            await _adminRepository.ThemDieuKhoanThanhPhan(dktp);
            return RedirectToAction("Luatgiaothongchitiet", new { id = idvb });
        }
        public async Task<IActionResult> AddNewTintuc()
        {
            bool trangthai = false;
            var currentUser = await _userManager.GetUserAsync(User);

            if (await _userManager.IsInRoleAsync(currentUser, "Admin"))
            {
                trangthai = true;
            }
            Tintuc tintuc = new Tintuc
            {
                Ngaycapnhat = DateTime.Now,
                Trangthai = trangthai
            };

            return View(tintuc);
        }
        [HttpPost]
        public async Task<IActionResult> AddNewTintuc(Tintuc tintuc, String filename)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            tintuc.Iduser = currentUser.Id;
            tintuc.Tenhinhanh = filename;
            await _adminRepository.AddNewtintuc(tintuc);
            TempData["Message"] = "Thêm tin tức thành công!";
            TempData["MessageType"] = "success";
            return RedirectToAction("Tintuc", "Admin");
        }

        public async Task<ActionResult> QuanlyBaidangTintuc()
        {
            var Tintuc = await _adminRepository.TintucKiemduyet();

            var Listusers = await _userManager.Users.ToListAsync();

            foreach (var item in Tintuc)
            {
                foreach (var user in Listusers)
                {
                    if (user.Id == item.Iduser)
                    {
                        item.Iduser = user.UserName;
                    }
                }
            }

            return View(Tintuc);
        }

        [HttpPost]
        public async Task<IActionResult> DeleteBanComment(string iduser)
        {
            var binhluanList = await _adminRepository.BinhluanKiemduyet();
            List<BinhLuan> Listbl = binhluanList.Where(p => p.IdUser.Contains(iduser)).ToList();

            await _adminRepository.Xoabinhluan(Listbl);
            TempData["Message"] = "Gỡ cấm bình luận thành công!";
            TempData["MessageType"] = "success";
            return RedirectToAction("QuanlyBinhluan", "Admin");
        }

        [HttpPost]
        public async Task<IActionResult> ApproveAllNews() // duyệt toàn bộ tin tức
        {
            var ListTintuc = await _adminRepository.TintucKiemduyet();

            foreach (var item in ListTintuc)
            {
                item.Trangthai = true;
                await _adminRepository.DuyetTintuc(item);
            }

            return RedirectToAction("QuanlyBaidangTintuc", "Admin");
        }
        [HttpPost]
        public async Task<IActionResult> ApproveNews(int id) // duyệt toàn bộ tin tức
        {
            var tintuc = await _adminRepository.Timkiemtintuc(id);


            tintuc.Trangthai = true;
            await _adminRepository.DuyetTintuc(tintuc);

            return RedirectToAction("QuanlyBaidangTintuc", "Admin");
        }
        public async Task<IActionResult> Vanban() // văn bản 
        {
            var ListVanban = await _adminRepository.GetallVanban();
            return View(ListVanban);
        }
        public async Task<IActionResult> DeleteVanban(int id)
        {
            var vanban = await _adminRepository.TimkiemVanban(id);
            return View(vanban);
        }
        [HttpPost]
        public async Task<IActionResult> CompleteDeleteVanban(int id) // xóa văn bản
        {

            await _adminRepository.DeleteVanban(id);

            TempData["Message"] = "Xóa văn bản thành công!";
            TempData["MessageType"] = "no-success";
            return RedirectToAction("Vanban", "Admin");
        }

        public async Task<IActionResult> Tintuc()
        {
            var ListTintuc = await _adminRepository.GetAllTintuc();
            var Listusers = await _userManager.Users.ToListAsync();

            // Thay thế Iduser bằng UserName
            foreach (var item in ListTintuc)
            {
                var user = Listusers.FirstOrDefault(u => u.Id == item.Iduser);
                if (user != null)
                {
                    item.Iduser = user.UserName;
                }
            }
            return View(ListTintuc);
        }

        public async Task<IActionResult> CompleteDeleteTintuc(int id) // xóa tin tức
        {
            var vanban = await _adminRepository.Timkiemtintuc(id);
            if (vanban != null)
            {
                await _adminRepository.DeleteTintuc(id);
                TempData["Message"] = "Xóa tin tức thành công!";
                TempData["MessageType"] = "success";
                return RedirectToAction("Tintuc", "Admin");
            }
            TempData["Message"] = "Xóa tin tức không thành công!";
            TempData["MessageType"] = "no-success";
            return RedirectToAction("Tintuc", "Admin");
        }
        public async Task<IActionResult> Tucam()
        {
            var tucamList = await _adminRepository.GetallTucam();
            return View(tucamList);
        }
		[HttpPost]
		public async Task<IActionResult> ThemTucam(TuCam tucam)
		{
			if (ModelState.IsValid)
			{
				bool check = await _adminRepository.themTucam(tucam);
                if(check)
                {
					TempData["Message"] = "Thêm từ cấm thành công!";
					TempData["MessageType"] = "success";
					return RedirectToAction("Tucam", "Admin");
                }
                else
                {
					TempData["Message"] = "Thêm từ cấm không thành công! Đã tồn tại từ cấm";
					TempData["MessageType"] = "success";
					return RedirectToAction("Tucam", "Admin");
				}
			}
			
			return RedirectToAction("Tucam", "Admin");
		}
		public async Task<IActionResult> Xoatucam(int id)
        {
            var tuCam = await _adminRepository.timkiemTucam(id);
            if (tuCam != null)
            {
                await _adminRepository.XoaTucam(tuCam);
                TempData["Message"] = "Xóa từ cấm thành công!";
                TempData["MessageType"] = "success";
                return RedirectToAction("Tucam", "Admin");
            }
            TempData["Message"] = "Xóa từ cấm không thành công!";
            TempData["MessageType"] = "no-success";
            return RedirectToAction("Tucam", "Admin");
        }

        private void ConvertToWav(string inputFile, string outputFile)
        {
            using (var reader = new MediaFoundationReader(inputFile))
            using (var resampler = new WaveFormatConversionStream(new WaveFormat(16000, 16, 1), reader)) // 16,000 Hz, 16-bit, mono
            using (var writer = new WaveFileWriter(outputFile, resampler.WaveFormat))
            {
                resampler.CopyTo(writer);
            }
        }

        //speech to text
        [HttpGet]
        public IActionResult UploadAudio()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UploadAudio(IFormFile audioFile)
        {
            if (audioFile == null || audioFile.Length == 0)
            {
                return BadRequest(new { message = "Cần có tệp âm thanh." });
            }

            try
            {
                // Logic lưu tệp và chuyển đổi âm thanh thành văn bản
                var uploadDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploads");
                if (!Directory.Exists(uploadDirectory))
                {
                    Directory.CreateDirectory(uploadDirectory);
                }

                var filePath = Path.Combine(uploadDirectory, audioFile.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await audioFile.CopyToAsync(stream);
                }

                var wavFilePath = Path.Combine(uploadDirectory, Path.GetFileNameWithoutExtension(audioFile.FileName) + "audio_converted.wav");
                ConvertToWav(filePath, wavFilePath);


                var text = await _adminRepository.ConvertSpeechToTextAsync(wavFilePath);
                return Ok(new { result = text });
            }
            catch (Exception ex)
            {
                // Trả về JSON báo lỗi chi tiết
                return StatusCode(500, new { error = "Có lỗi xảy ra khi xử lý tệp âm thanh.", details = ex.ToString() });
            }
        }
        [HttpGet]
        public IActionResult UploadAndExtractText()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UploadAndExtractText(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return BadRequest(new { message = "Vui lòng chọn một tệp ảnh hoặc PDF." });

            try
            {
                var uploadDirectory = Path.Combine(Path.GetTempPath(), "uploads");
                if (!Directory.Exists(uploadDirectory))
                {
                    Directory.CreateDirectory(uploadDirectory);
                }

                var filePath = Path.Combine(uploadDirectory, file.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                // Trích xuất văn bản từ file
                var extractedText = await _adminRepository.ExtractTextFromFileAsync(filePath);

                // Trả về kết quả trích xuất dưới dạng JSON
                return Ok(new { extractedText = extractedText });
            }
            catch (Exception ex)
            {
                // Log chi tiết lỗi
                Console.WriteLine($"Error: {ex.Message}\nStack Trace: {ex.StackTrace}");

                // Trả về thông báo lỗi cho client
                return StatusCode(500, new { error = "Có lỗi xảy ra khi xử lý tệp." });
            }
        }

        ///gọi api  API OCR.Space
        /*public async Task<IActionResult> UploadAndExtractText(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return BadRequest(new { message = "Vui lòng chọn một tệp ảnh hoặc PDF." });

            try
            {
                var uploadDirectory = Path.Combine(Path.GetTempPath(), "uploads");
                if (!Directory.Exists(uploadDirectory))
                {
                    Directory.CreateDirectory(uploadDirectory);
                }

                var filePath = Path.Combine(uploadDirectory, file.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }

                // Trích xuất văn bản từ file bằng OCR.Space
                var extractedText = await _adminRepository.ExtractTextFromImageOCRSpaceAsync(filePath);

              *//*  var correctedText = CorrectVietnameseText(extractedText);*//*

                // Trả về kết quả trích xuất dưới dạng JSON
                return Ok(new { extractedText = extractedText });
            }
            catch (Exception ex)
            {
                // Log chi tiết lỗi
                Console.WriteLine($"Error: {ex.Message}\nStack Trace: {ex.StackTrace}");

                // Trả về thông báo lỗi cho client
                return StatusCode(500, new { error = "Có lỗi xảy ra khi xử lý tệp." });
            }
        }*/
        /* public string CorrectVietnameseText(string inputText)
         {
             // Tiến hành chuyển đổi văn bản không dấu thành có dấu.
             return AddVietnameseAccents(inputText);
         }

         private string AddVietnameseAccents(string text)
         {
             var replacements = new Dictionary<char, string>
         {
             { 'a', "àáảãạăắằẳẵặâấầẩẫậ" },
             { 'e', "èéẻẽẹêếềểễệ" },
             { 'i', "ìíỉĩị" },
             { 'o', "òóỏõọôốồổỗộơớờởỡợ" },
             { 'u', "ùúủũụưứừửữự" },
             { 'y', "ỳýỷỹỵ" },
             { 'd', "đ" }
         };

             var result = string.Empty;

             foreach (var character in text)
             {
                 if (replacements.ContainsKey(character))
                 {
                     result += replacements[character];
                 }
                 else
                 {
                     result += character;
                 }
             }

             return result;
         }

 */


        [HttpGet]
        public IActionResult ListBlobs()
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("pictures-tintuc");
            containerClient.CreateIfNotExists();

            var blobs = containerClient.GetBlobs();

            return View(blobs);
        }

        // Upload file
        [HttpPost]
        public async Task<String> Upload(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return "No file selected!";

            var containerClient = _blobServiceClient.GetBlobContainerClient("pictures-tintuc");
            await containerClient.CreateIfNotExistsAsync();

            var blobClient = containerClient.GetBlobClient(file.FileName);
            await blobClient.UploadAsync(file.OpenReadStream());

            return file.FileName;
        }
        [HttpGet]
        public async Task<IActionResult> GetImage(string blobName)
        {
            var containerClient = _blobServiceClient.GetBlobContainerClient("pictures-tintuc");
            var blobClient = containerClient.GetBlobClient(blobName);

            if (await blobClient.ExistsAsync())
            {
                var stream = await blobClient.OpenReadAsync();
                return File(stream, "image/jpeg"); // Điều chỉnh MIME type nếu cần
            }

            return NotFound();
        }

    }
}
