﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json.Serialization;
using System.Text.Json;
using WebTracuuLuatGiaoThong.Models;
using WebTracuuLuatGiaoThong.Repository.Admin;
using WebTracuuLuatGiaoThong.Repository.User;
using X.PagedList;

namespace WebTracuuLuatGiaoThong.Controllers.API_Android
{
	[ApiController]
	[Route("api/[controller]")]
	public class API_Android : ControllerBase
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IUserRepository _userRepository;
		private readonly IAdminRepository _adminRepository;

		public API_Android(IUserRepository userRepository, UserManager<ApplicationUser> userManager, IAdminRepository adminRepository)
		{
			_userRepository = userRepository;
			_userManager = userManager;
			_adminRepository = adminRepository;
		}
		[HttpGet("Index")]
		public async Task<IActionResult> GetIndex() // API để lấy danh sách tin tức và văn bản mới
		{
			var Listtintuc = await _userRepository.GetTintucMoiAsync();
			var ListVanban = await _userRepository.GetVanBanMoiAsync();
			var Listusers = await _userManager.Users.ToListAsync();

			foreach (var item in Listtintuc)
			{
				var user = Listusers.FirstOrDefault(u => u.Id == item.Iduser);
				if (user != null)
				{
					item.Iduser = user.UserName;
				}
			}

			return Ok(new { TinTucMoi = Listtintuc, VanBanMoi = ListVanban });
		}

		[HttpGet("Displaytintuc/{id}")]
		public async Task<IActionResult> GetDisplayTintuc(int id) // API để hiển thị chi tiết tin tức
		{
			var tintuc = await _userRepository.GetTintucByID(id);
			if (tintuc == null) return NotFound();

			var Listusers = await _userManager.Users.ToListAsync();
			var Listbinhluan = await _userRepository.GetBinhluantintuc(id);

			var user = Listusers.FirstOrDefault(u => u.Id == tintuc.Iduser);
			if (user != null)
			{
				tintuc.Iduser = user.UserName;
			}

			foreach (var binhluan in Listbinhluan)
			{
				var userBinhluan = Listusers.FirstOrDefault(u => u.Id == binhluan.IdUser);
				if (userBinhluan != null)
				{
					binhluan.IdUser = userBinhluan.UserName;
				}
			}

			return Ok(new { TinTuc = tintuc, BinhLuans = Listbinhluan });
		}

		[HttpGet("Vanbanmoi")]
		public async Task<IActionResult> GetVanBanMoi(int? page = 1, int? pageSize = 5, string searchText = null, string filterBy = null, DateTime? ngayBanHanh = null) // API cho văn bản mới
		{
			var ListVanban = await _userRepository.GetVanBanMoiAsync();

			if (!string.IsNullOrEmpty(searchText))
			{
				searchText = searchText.Trim().ToLower();
				if (!string.IsNullOrEmpty(filterBy))
				{
					switch (filterBy.ToLower())
					{
						case "coquan":
							ListVanban = ListVanban.Where(vb => vb.Coquanphathanh != null && vb.Coquanphathanh.ToLower().Contains(searchText)).ToList();
							break;
						case "sohieu":
							ListVanban = ListVanban.Where(vb => vb.Sohieuvanban != null && vb.Sohieuvanban.ToLower().Contains(searchText)).ToList();
							break;
						case "tenvanban":
							ListVanban = ListVanban.Where(vb => vb.Ten != null && vb.Ten.ToLower().Contains(searchText)).ToList();
							break;
						case "ngaybanhanh":
							if (ngayBanHanh.HasValue)
							{
								var ngayBanHanhDateOnly = DateOnly.FromDateTime(ngayBanHanh.Value);
								ListVanban = ListVanban.Where(vb => vb.Ngaybanhanh == ngayBanHanhDateOnly).ToList();
							}
							break;
					}
				}
			}

			var pagedVanBan = ListVanban.ToPagedList((int)page, (int)pageSize);
			return Ok(pagedVanBan);
		}

		[HttpGet("Tintucmoi")]
		public async Task<IActionResult> GetTintucmoi(int? page = 1, int? pageSize = 10, string searchText = null) // API cho tin tức mới
		{
			var Listtintuc = await _userRepository.GetTintucMoiAsync();
			var Listusers = await _userManager.Users.ToListAsync();

			foreach (var item in Listtintuc)
			{
				var user = Listusers.FirstOrDefault(u => u.Id == item.Iduser);
				if (user != null)
				{
					item.Iduser = user.UserName;
				}
			}

			if (!string.IsNullOrEmpty(searchText))
			{
				searchText = searchText.Trim().ToLower();
				Listtintuc = Listtintuc.Where(tt =>
					(tt.Ten != null && tt.Ten.ToLower().Contains(searchText)) ||
					(tt.Iduser != null && tt.Iduser.ToLower().Contains(searchText))
				).ToList();
			}

			var pagedTinTuc = Listtintuc.ToPagedList((int)page, (int)pageSize);
			return Ok(pagedTinTuc);
		}
		[HttpGet("api/luatgiaothong")]
		public async Task<ActionResult<IEnumerable<Vanban>>> GetLuatgiaothong(int? page, int? pageSize, string searchText)
		{
			int currentPage = page ?? 1;
			int currentPageSize = pageSize ?? 10;

			var Listluatgiaothong = await _userRepository.GetAllVanBanLuatAsync();

			if (!string.IsNullOrEmpty(searchText))
			{
				searchText = searchText.Trim().ToLower();

				Listluatgiaothong = Listluatgiaothong.Where(vb1 =>
					(vb1.Ten != null && vb1.Ten.ToLower().Contains(searchText)) ||
					(vb1.Sohieuvanban != null && vb1.Sohieuvanban.ToLower().Contains(searchText))).ToList();
			}

			var pagedLuatgiaothong = Listluatgiaothong.Skip((currentPage - 1) * currentPageSize).Take(currentPageSize);

			return Ok(pagedLuatgiaothong);
		}
		[HttpGet("api/luatgiaothong/{id}")]
		public async Task<ActionResult> GetLuatgiaothongchitiet(int id)
		{
			var noidung = await _userRepository.GetNoidungcuaVanban(id);
			var note = await _userRepository.viewNote(_userManager.GetUserId(User));

			return Ok(new
			{
				Noidung = noidung,
				NoteList = note
			});
		}

		[HttpPost("api/luatgiaothong/dieuluat/ghichu")]
		public async Task<ActionResult> AddGhiChuDieuluat([FromBody] string noidung, [FromQuery] int IDdieuluat)
		{
			if (string.IsNullOrEmpty(noidung))
			{
				return BadRequest("Nội dung ghi chú không được để trống.");
			}

			Ghichu ghichu = new Ghichu
			{
				Noidung = noidung,
				Iddl = IDdieuluat,
				Iduser = _userManager.GetUserId(User)
			};

			bool luughichu = await _userRepository.addNote(ghichu);

			if (luughichu)
			{
				return Ok("Ghi chú đã được thêm thành công.");
			}

			return StatusCode(500, "Không thể thêm ghi chú.");
		}
		[HttpPost("api/luatgiaothong/dieukhoan/ghichu")]
		public async Task<ActionResult> AddGhiChuDieukhoan([FromBody] string noidung, [FromQuery] int IDDieukhoan)
		{
			if (string.IsNullOrEmpty(noidung))
			{
				return BadRequest("Nội dung ghi chú không được để trống.");
			}

			Ghichu ghichu = new Ghichu
			{
				Noidung = noidung,
				Iddk = IDDieukhoan,
				Iduser = _userManager.GetUserId(User)
			};

			bool luughichu = await _userRepository.addNote(ghichu);

			if (luughichu)
			{
				return Ok("Ghi chú đã được thêm thành công.");
			}

			return StatusCode(500, "Không thể thêm ghi chú.");
		}
		[HttpPost]
		[Route("api/notes/sub-clause")]
		public async Task<IActionResult> ThemghichuDieukhoantp([FromBody] string noidung, int idDieukhoantp)
		{
			if (string.IsNullOrEmpty(noidung))
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(idDieukhoantp, 3);
				return BadRequest(new { message = "Nội dung ghi chú là bắt buộc.", vanBanId = vanBan });
			}

			var ghichu = new Ghichu
			{
				Noidung = noidung,
				Iddktp = idDieukhoantp,
				Iduser = _userManager.GetUserId(User)
			};

			bool success = await _userRepository.addNote(ghichu);
			if (success)
			{
				return Ok(new { message = "Thêm ghi chú thành công." });
			}
			else
			{
				return StatusCode(500, new { message = "Lỗi khi thêm ghi chú." });
			}
		}
		[HttpPut]
		[Route("api/notes/{idghichu}")]
		public async Task<IActionResult> UpdateOrDeleteNote(int idghichu, [FromBody] string noidung, int idVanban)
		{
			if (string.IsNullOrEmpty(noidung))
			{
				var ktra = await _userRepository.deleteNote(idghichu);
				return ktra
					? Ok(new { message = "Xóa ghi chú thành công." })
					: StatusCode(500, new { message = "Lỗi khi xóa ghi chú." });
			}

			var ghichu = await _userRepository.viewNotebyID(idghichu);
			ghichu.Noidung = noidung;

			bool success = await _userRepository.updateNote(ghichu);
			return success
				? Ok(new { message = "Chỉnh sửa ghi chú thành công." })
				: StatusCode(500, new { message = "Lỗi khi chỉnh sửa ghi chú." });
		}
		[HttpGet]
		[Route("api/search")]
		public async Task<IActionResult> Search(string text)
		{
			var result = await _userRepository.Find(text);
			var Listusers = await _userManager.Users.ToListAsync();

			foreach (var item in result.Item1)
			{
				foreach (var user in Listusers)
				{
					if (user.Id == item.Iduser)
					{
						item.Iduser = user.UserName;
					}
				}
			}

			if (result.Item1 == null && result.Item2 == null)
			{
				return NotFound(new { message = "Không tìm thấy văn bản hay tin tức nào." });
			}

			return Ok(result);
		}

		private async Task<bool> KiemtraUser(string userID)
		{
			var binhluanList = await _userRepository.BinhluanKiemduyet(); // danh sách bình luận có trạng thái = false
			if (binhluanList == null)
				return true; // được bình luận

			var ListBanUserID = binhluanList // list user bị cấm bình luận
				.GroupBy(b => b.IdUser)
				.Where(g => g.Count() >= 3)
				.Select(g => g.Key)
				.ToList();


			return !ListBanUserID.Contains(userID);
		}

		[HttpPost]
		[Route("api/comments")]
		public async Task<IActionResult> PostComment([FromBody] BinhLuan binhLuan)
		{
			if (!await KiemtraUser(binhLuan.IdUser))
			{
				/*return Forbid(new { message = "Bạn đã bị cấm bình luận do vi phạm quy tắc 3 lần!" });*/
			}

			var Listcam = await _userRepository.GetallTucam();
			foreach (var tuCam in Listcam)
			{
				if (binhLuan.Noidung.Contains(tuCam.Noidung))
				{
					binhLuan.TrangThai = false;
					break;
				}
			}

			binhLuan.TrangThai = true;

			await _userRepository.SaveBinhluan(binhLuan);

			return Ok(new { message = "Bình luận thành công." });
		}


	}


}

