﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using WebTracuuLuatGiaoThong.Models;
using WebTracuuLuatGiaoThong.Repository.Admin;
using WebTracuuLuatGiaoThong.Repository.User;
using System;
using System.IO;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using X.PagedList;
using System.Text;
using NAudio.Wave;


namespace WebTracuuLuatGiaoThong.Controllers.User
{
	public class UserController : Controller
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IUserRepository _userRepository;
		private readonly IAdminRepository _adminRepository;
		public UserController(IUserRepository userRepository, UserManager<ApplicationUser> userManager, IAdminRepository adminRepository)
		{
			_userRepository = userRepository;
			_userManager = userManager;
			_adminRepository = adminRepository;
		}
        public IActionResult CreateDocument()
        {
            return View();
        }

        // Phương thức để xử lý form khi người dùng gửi
        [HttpPost]
        public IActionResult CreateDocument(string tenCoQuan, string soVanBan, string ngayBanHanh, string noiDung)
        {
            // Tạo nội dung cho văn bản
            StringBuilder documentContent = new StringBuilder();
            documentContent.AppendLine($"Tên cơ quan: {tenCoQuan}");
            documentContent.AppendLine($"Số văn bản: {soVanBan}");
            documentContent.AppendLine($"Ngày ban hành: {ngayBanHanh}");
            documentContent.AppendLine($"Nội dung: {noiDung}");

            // Lưu thông tin vào ViewBag
            ViewBag.TenCoQuan = tenCoQuan;
            ViewBag.SoVanBan = soVanBan;
            ViewBag.NgayBanHanh = ngayBanHanh;
            ViewBag.DocumentContent = documentContent.ToString(); // Đưa nội dung vào ViewBag

            return View("DisplayDocument"); // Trả về view hiển thị nội dung văn bản
        }




        public async Task<IActionResult> Index()//trang chủ trả về cho user tin tức và văn bản mới
		{
			var Listtintuc = await _userRepository.GetTintucMoiAsync();//danh tin tức mới
			var ListVanban = await _userRepository.GetVanBanMoiAsync();//danh sách văn bản mới

			/*var Listusers = await _userManager.Users.ToListAsync();*/

			foreach (var item in Listtintuc)
			{
				/*foreach (var user in Listusers)
                {
                    if (user.Id == item.Iduser)
                    {
                        item.Iduser = user.UserName;
                    }
                }*/
			}

			return View((Listtintuc.ToList(), ListVanban.ToList()));
		}

		public async Task<IActionResult> Displaytintuc(int id)//hiển thị chi tiết tin tức mới
		{
			var tintuc = await _userRepository.GetTintucByID(id);
			var Listusers = await _userManager.Users.ToListAsync();
			var Listbinhluan = await _userRepository.GetBinhluantintuc(id);
			foreach (var user in Listusers)
			{
				if (user.Id == tintuc.Iduser)
				{
					tintuc.Iduser = user.UserName;
				}
			}
			foreach (var binhluan in Listbinhluan)
			{
				foreach (var user in Listusers)
				{
					if (binhluan.IdUser == user.Id)
					{
						binhluan.IdUser = user.UserName;
					}
				}
			}
			ViewBag.Binhluans = Listbinhluan;
			return View(tintuc);
		}
		//public async Task<IActionResult> Vanbanmoi() // văn bản mới
		//{
		//    var ListVanban = await _userRepository.GetVanBanMoiAsync();
		//    return View(ListVanban);
		//}

		//public async Task<ActionResult> Vanbanmoi(int? page, int? pageSize, string searchText)
		//{
		//    if (page == null)
		//    {
		//        page = 1;
		//    }
		//    if (pageSize == null)
		//    {
		//        pageSize = 5;
		//    }
		//    var ListVanban = await _userRepository.GetVanBanMoiAsync(); // Lấy danh sách văn bản mới từ repository			
		//                                                                // Sử dụng ToPagedList để phân trang
		//    if (!string.IsNullOrEmpty(searchText))
		//    {
		//        searchText = searchText.Trim().ToLower(); // Chuẩn hóa chuỗi tìm kiếm

		//        ListVanban = ListVanban.Where(vb =>
		//            (vb.Sohieuvanban != null && vb.Sohieuvanban.ToLower().Contains(searchText)) ||
		//            (vb.Coquanphathanh != null && vb.Coquanphathanh.ToLower().Contains(searchText)) ||
		//            (vb.Ten != null && vb.Ten.ToLower().Contains(searchText))
		//        ).ToList();
		//    }

		//    var pagedVanBan = ListVanban.ToPagedList((int)page, (int)pageSize);

		//    return View(pagedVanBan);
		//}

		public async Task<ActionResult> Vanbanmoi(int? page, int? pageSize, string searchText, string filterBy)
		{
			if (page == null)
			{
				page = 1;
			}
			if (pageSize == null)
			{
				pageSize = 6;
			}

			var ListVanban = await _userRepository.GetVanBanMoiAsync(); // Lấy danh sách văn bản mới từ repository

			// Chuẩn hóa chuỗi tìm kiếm
			if (!string.IsNullOrEmpty(searchText))
			{
				searchText = searchText.Trim().ToLower(); // Chuẩn hóa chuỗi tìm kiếm
				// Lọc theo tiêu chí đã chọn
				if (!string.IsNullOrEmpty(filterBy))
				{
					switch (filterBy)
					{
						case "coquan": // Lọc theo cơ quan
							ListVanban = ListVanban.Where(vb =>
								vb.Coquanphathanh != null && vb.Coquanphathanh.ToLower().Contains(searchText)).ToList();
							break;
						case "sohieu": // Lọc theo số hiệu
							ListVanban = ListVanban.Where(vb =>
								vb.Sohieuvanban != null && vb.Sohieuvanban.ToLower().Contains(searchText)).ToList();
							break;
						case "tenvanban": // Lọc theo tên văn bản
							ListVanban = ListVanban.Where(vb =>
								vb.Ten != null && vb.Ten.ToLower().Contains(searchText)).ToList();
							break;
						default:
							break;
					}
				}
			}
			var pagedVanBan = ListVanban.ToPagedList((int)page, (int)pageSize);
			// Để giữ lại giá trị của biến searchText và filterBy trong view
			ViewBag.SearchText = searchText;
			ViewBag.FilterBy = filterBy;
			return View(pagedVanBan);	
		}	

		public async Task<IActionResult> Tintucmoi(int? page, int? pageSize, string searchText) // Phân trang cho tin tức mới
		{
			int currentPage = page ?? 1;
			int currentPageSize = pageSize ?? 6;

			var Listtintuc = await _userRepository.GetTintucMoiAsync();
			var Listusers = await _userManager.Users.ToListAsync();

			// Thay thế Iduser bằng UserName
			foreach (var item in Listtintuc)
			{
				var user = Listusers.FirstOrDefault(u => u.Id == item.Iduser);
				if (user != null)
				{
					item.Iduser = user.UserName;
				}
			}
			if (!string.IsNullOrEmpty(searchText))
			{
				searchText = searchText.Trim().ToLower(); // Chuẩn hóa chuỗi tìm kiếm

				Listtintuc = Listtintuc.Where(tt =>
					(tt.Ten != null && tt.Ten.ToLower().Contains(searchText)) ||
					(tt.Iduser != null && tt.Iduser.ToLower().Contains(searchText)) // Tìm kiếm theo UserName
				).ToList();
			}

			var pagedTinTuc = Listtintuc.ToPagedList(currentPage, currentPageSize);

			return View(pagedTinTuc);
		}
		public async Task<IActionResult> Luatgiaothong(int? page, int? pageSize, string searchText)//hiển thị toàn bộ văn bản luật giao thông đường độ
		{

			int currentPage = page ?? 1; // 
			int currentPageSize = pageSize ?? 10;

			var Listluatgiaothong = await _userRepository.GetAllVanBanLuatAsync();
			if (!string.IsNullOrEmpty(searchText))
			{
				searchText = searchText.Trim().ToLower(); // Chuẩn hóa chuỗi tìm kiếm

				Listluatgiaothong = Listluatgiaothong.Where(vb1 =>
					(vb1.Ten != null && vb1.Ten.ToLower().Contains(searchText)) ||
					(vb1.Sohieuvanban != null && vb1.Sohieuvanban.ToLower().Contains(searchText))).ToList();
			}

			var pagedLuatgiaothong = Listluatgiaothong.ToPagedList(currentPage, currentPageSize);
			return View(pagedLuatgiaothong);
		}
		public async Task<IActionResult> Luatgiaothongchitiet(int id)//lấy toàn bộ nội dung của văn bản
		{
			var noidung = await _userRepository.GetNoidungcuaVanban(id);
			var note = await _userRepository.viewNote(_userManager.GetUserId(User));
			if (note != null)
			{
				ViewBag.NoteList = note;
				return View(noidung);
			}
			else
			{
				ViewBag.NoteList = null;
				return View(noidung);
			}

		}
		[HttpPost]
		public async Task<IActionResult> ThemghichuDieuluat(string noidung, int IDdieuluat)//thêm ghi chú cho điều luật
		{
			Ghichu ghichu = new Ghichu();
			if (noidung == null)
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDdieuluat, 1);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
			ghichu.Noidung = noidung;
			ghichu.Iddl = IDdieuluat;
			ghichu.Iduser = _userManager.GetUserId(User);
			var luughichu = await _userRepository.addNote(ghichu);

			if (luughichu)
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDdieuluat, 1);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
			else
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDdieuluat, 1);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
		}
		[HttpPost]
		public async Task<IActionResult> ThemghichuDieukhoan(string noidung, int IDDieukhoan)//thêm ghi chú cho điều khoản
		{
			Ghichu ghichu = new Ghichu();
			if (noidung == null)
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDDieukhoan, 2);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
			ghichu.Noidung = noidung;
			ghichu.Iddk = IDDieukhoan;
			ghichu.Iduser = _userManager.GetUserId(User);
			var luughichu = await _userRepository.addNote(ghichu);
			if (luughichu == true)
			{

				var vanBan = await _userRepository.GetVanBanByIDAsync(IDDieukhoan, 2);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
			else
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDDieukhoan, 2);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
		}

		[HttpPost]
		public async Task<IActionResult> ThemghichuDieukhoantp(string noidung, int IDDieukhoantp)//thêm ghi chú cho điều khoản thành phần
		{
			Ghichu ghichu = new Ghichu();
			ghichu.Noidung = noidung;
			if (noidung == null)
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDDieukhoantp, 3);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
			ghichu.Iddktp = IDDieukhoantp;
			ghichu.Iduser = _userManager.GetUserId(User);
			var luughichu = await _userRepository.addNote(ghichu);
			if (luughichu == true)
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDDieukhoantp, 3);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
			else
			{
				var vanBan = await _userRepository.GetVanBanByIDAsync(IDDieukhoantp, 3);
				return RedirectToAction("Luatgiaothongchitiet", new { id = vanBan });
			}
		}

		[HttpPost]
		public async Task<IActionResult> Chinhsuaghichu(string noidung, int idghichu, int idVanban)//chính sửa ghi chú muốn chỉnh sửa
		{
			if (noidung == null)
			{
				var ktra = await _userRepository.deleteNote(idghichu);
				return RedirectToAction("Luatgiaothongchitiet", new { id = idVanban });
			}
			var ghichu = await _userRepository.viewNotebyID(idghichu);
			ghichu.Noidung = noidung;

			var luughichu = await _userRepository.updateNote(ghichu);
			if (luughichu == true)
			{
				return RedirectToAction("Luatgiaothongchitiet", new { id = idVanban });
			}
			else
			{
				return RedirectToAction("Luatgiaothongchitiet", new { id = idVanban });
			}
		}
		[HttpGet]
		public async Task<IActionResult> TimKiem(string text)
		{
			var result = await _userRepository.Find(text);
			var Listusers = await _userManager.Users.ToListAsync();

			foreach (var item in result.Item1)
			{
				foreach (var user in Listusers)
				{
					if (user.Id == item.Iduser)
					{
						item.Iduser = user.UserName;
					}
				}
			}
			if (result.Item1 == null && result.Item2 == null)
			{
				TempData["Message"] = "Không tồn tại văn bản hay tin tức nào!";
				TempData["MessageType"] = "success";
				return RedirectToAction("Index", "User");
			}
			return View(result);
		}
		public IActionResult LoginModal()
		{
			return PartialView("~/Areas/Identity/Pages/Account/Login.cshtml");
		}

		private async Task<bool> KiemtraUser(string userID)
		{
			var binhluanList = await _userRepository.BinhluanKiemduyet(); // danh sách bình luận có trạng thái = false
			if (binhluanList == null)
				return true; // được bình luận

			var ListBanUserID = binhluanList // list user bị cấm bình luận
				.GroupBy(b => b.IdUser)
				.Where(g => g.Count() >= 3)
				.Select(g => g.Key)
				.ToList();


			return !ListBanUserID.Contains(userID);
		}

		[HttpPost]
		public async Task<IActionResult> Binhluan(BinhLuan binhLuan)//bình luận
		{
			if (await KiemtraUser(binhLuan.IdUser) == false)
			{
				TempData["Message"] = "Bạn đã bị cấm bình luận vì đã bình luận với từ khiếm nhã 3 lần!";
				TempData["MessageType"] = "success";
				return RedirectToAction("Displaytintuc", new { id = binhLuan.IdTintuc });
			}
			List<TuCam> Listcam = await _userRepository.GetallTucam();
			foreach (var tuCam in Listcam)
			{
				if (binhLuan.Noidung.Contains(tuCam.Noidung))
				{
					binhLuan.TrangThai = false;
				}
			}
			binhLuan.TrangThai = true;

			await _userRepository.SaveBinhluan(binhLuan);
			TempData["Message"] = "Đã bình luận thành công!";
			TempData["MessageType"] = "success";
			return RedirectToAction("Displaytintuc", new { id = binhLuan.IdTintuc });

		}
		public async Task<IActionResult> AddNewTintuc(string idUser)
		{
			bool trangthai = false;
			var currentUser = await _userManager.GetUserAsync(User);

			if (await _userManager.IsInRoleAsync(currentUser, "Admin"))
			{
				trangthai = true;
			}
			Tintuc tintuc = new Tintuc
			{
				Iduser = idUser,
				Ngaycapnhat = DateTime.Now,
				Trangthai = trangthai
			};

			return View(tintuc);
		}
		[HttpPost]
		public async Task<IActionResult> AddNewTintuc(Tintuc tintuc, String filename)
		{
			tintuc.Tenhinhanh = filename;
			await _adminRepository.AddNewtintuc(tintuc);
			var currentUser = await _userManager.GetUserAsync(User);
			if (await _userManager.IsInRoleAsync(currentUser, "Admin"))
			{
				TempData["Message"] = "Tạo tin tức mới thành công!";
				TempData["MessageType"] = "success";
				return RedirectToAction("Tintucmoi", "User");
			}

			TempData["Message"] = "Đã gửi tin tức mới thành công.Hãy đợi kiểm duyệt!";
			TempData["MessageType"] = "success";
			return RedirectToAction("Index", "User");
		}
        private void SetDefaultFont(MainDocumentPart mainPart)
        {
            // Kiểm tra nếu phần StyleDefinitionsPart chưa tồn tại
            if (mainPart.StyleDefinitionsPart == null)
            {
                mainPart.AddNewPart<StyleDefinitionsPart>();
                mainPart.StyleDefinitionsPart.Styles = new Styles();
            }

            var styles = mainPart.StyleDefinitionsPart.Styles;

            // Xóa các phần tử Fonts mặc định cũ nếu có
            styles.RemoveAllChildren<RunPropertiesDefault>();

            // Thêm Fonts mặc định mới
            RunPropertiesDefault runPropertiesDefault = new RunPropertiesDefault(
                new RunPropertiesBaseStyle(
                    new RunFonts { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }
                )
            );

            styles.Append(runPropertiesDefault);
            styles.Save();
        }

        public async Task<IActionResult> SaveFile(int idvb)
		{
			var VB = await _userRepository.GetNoidungcuaVanban(idvb);
			var Loai = await _userRepository.GetTenloai(VB.Item1.IdloaiVanBan);
			String tenloai = Loai.TenLoaivb;
			var vanban = VB.Item1;
			var chuongs = VB.Item2;
			var dieuluats = VB.Item3;
			var dieukhoans = VB.Item4;
			var dieukhoanthanhphans = VB.Item5;
			var bosung = VB.Item6;

			List<BosungVanban> cancu = new List<BosungVanban>();
            List<BosungVanban> noinhan = new List<BosungVanban>();

            foreach (var item in bosung)
			{
				if(item == null) continue;
				else
				{
					if(item.IdloaiBs == 1)
					{
						cancu.Add(item);
					}
					else
					{
						noinhan.Add(item);
					}	
				}
			}	

			string userDocumentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			string validFileName = $"VanbanWebsiteTracuuLuatGiaothongVietNam.docx";
			string filePath = Path.Combine(userDocumentsPath, validFileName);

			int counter = 1;
			while (System.IO.File.Exists(filePath))
			{
				validFileName = $"VanbanWebsiteTracuuLuatGiaothongVietNam_{counter}.docx";
				filePath = Path.Combine(userDocumentsPath, validFileName);
				counter++;
			}

			using (WordprocessingDocument wordDocument = WordprocessingDocument.Create(filePath, WordprocessingDocumentType.Document))
			{
				MainDocumentPart mainPart = wordDocument.AddMainDocumentPart();
				mainPart.Document = new Document();
				Body body = new Body();

                SetDefaultFont(mainPart);

                AddHeader(body, vanban, tenloai, cancu);

				foreach (var chuong in chuongs)
				{
					AddContent(body, chuong, dieuluats, dieukhoans, dieukhoanthanhphans);
				}

				AddFooter(body, vanban, noinhan);
				// Lưu lại các thay đổi
				mainPart.Document.Append(body);
				mainPart.Document.Save();
			}

			TempData["Message"] = "Tải văn bản thành công! Lưu trữ tại Documents";
			TempData["MessageType"] = "success";
			return RedirectToAction("Luatgiaothongchitiet", "User", new { id = vanban.Idvb });
		}
		private void AddHeader(Body body, Vanban vanban, string tenloai, List<BosungVanban> cancu)
		{
			// Tạo bảng để chứa header
			DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();
			TableProperties tableProperties = new TableProperties(
				new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct });
			table.AppendChild(tableProperties);

			// Tạo hàng mới của bảng
			TableRow tr = new TableRow();

			// Tạo ô thứ nhất của hàng và thêm nội dung
			TableCell tc1 = new TableCell();
			Paragraph paraHeader1 = new Paragraph(new ParagraphProperties(new Bold(), new Justification() { Val = JustificationValues.Center }));

			if (vanban.Coquanphathanh.Length < ("Số: " + vanban.Sohieuvanban).Length)
			{
				paraHeader1.Append(new Run(new RunProperties(new Bold(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }), new Text(vanban.Coquanphathanh)));
				paraHeader1.Append(new Break());
				paraHeader1.Append(new Run(new RunProperties(new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }), new Text("Số: " + vanban.Sohieuvanban)));
			}
			else
			{
				paraHeader1.Append(new Run(new RunProperties(new Bold(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }), new Text(vanban.Coquanphathanh)));
				paraHeader1.Append(new Break());
				paraHeader1.Append(new Run(new RunProperties(new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }), new Text("Số: " + vanban.Sohieuvanban)));
			}

			tc1.Append(paraHeader1);
			tr.Append(tc1);

			// Tạo ô thứ hai của hàng và thêm tiêu đề
			TableCell tc2 = new TableCell(new TableCellProperties(new GridSpan() { Val = 3 }));
			Paragraph paraHeader2 = new Paragraph( new ParagraphProperties(new Justification() { Val = JustificationValues.Right }));

			// Dòng đầu tiên
			Run run1 = new Run(new RunProperties(new Bold(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
			run1.Append(new Text("CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM"));
			paraHeader2.Append(run1);

			// Xuống dòng
			paraHeader2.Append(new Break());
			string nonBreakingSpaces = new string('\u00A0', 14);

			// Dòng thứ hai
			Run run2 = new Run(new RunProperties(new Bold(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
			Text text2 = new Text("Độc lập - Tự do - Hạnh phúc" + nonBreakingSpaces);
			text2.Space = SpaceProcessingModeValues.Preserve;
			run2.Append(text2);
			paraHeader2.Append(run2);

			// Xuống dòng
			paraHeader2.Append(new Break());
			Run run3 = new Run(new RunProperties(new FontSize() { Val = "28" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
			run3.Append(new Text(vanban.Noibanhanh + ", ngày " + vanban.Ngaybanhanh.Day + " tháng " + vanban.Ngaybanhanh.Month + " năm " + vanban.Ngaybanhanh.Year));
			paraHeader2.Append(run3);

			tc2.Append(paraHeader2);
			tr.Append(tc2);

			// Thêm hàng vào bảng
			table.Append(tr);

			// Thêm bảng vào body
			body.Append(table);


			Paragraph paraHeader4 = new Paragraph(new ParagraphProperties(new Justification() { Val = JustificationValues.Center }));
			Run runHeader1 = new Run(new RunProperties(new Bold(), new FontSize() { Val = "28" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
			runHeader1.Append(new Text(tenloai));
			paraHeader4.Append(runHeader1);
			body.Append(paraHeader4);

			Paragraph paraHeader5 = new Paragraph(new ParagraphProperties(new Justification() { Val = JustificationValues.Center }));
			Run runHeader2 = new Run(new RunProperties(new Bold(), new FontSize() { Val = "28" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
			runHeader2.Append(new Text(vanban.Ten));
			paraHeader5.Append(runHeader2);
			body.Append(paraHeader5);

			if(cancu != null)
			{
                for (int i = 0; i < cancu.Count; i++)
                {
                    Paragraph paraHeader6 = new Paragraph(new ParagraphProperties(new Justification() { Val = JustificationValues.Left }));
                    Run runHeader3 = new Run(new RunProperties(new Italic(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));

                    string separator = (i == cancu.Count - 1) ? "." : ";";
                    runHeader3.Append(new Text(cancu[i].Noidung + separator));
                    paraHeader6.Append(runHeader3);
                    body.Append(paraHeader6);
                }
            }

        }

		private void AddContent(Body body, Chuong chuong, List<DieuLuat> dieuluats, List<DieuKhoan> dieukhoans, List<DieuKhoanThanhPhan> dieukhoanthanhphans)
		{
			// Thêm tiêu đề chương
			Paragraph paraChuong = new Paragraph();
			ParagraphProperties paraPropsChuong = new ParagraphProperties(new Justification() { Val = JustificationValues.Center });
			paraChuong.Append(paraPropsChuong);

			Run runChuong = new Run(new RunProperties(new Bold(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
			runChuong.Append(new Text(chuong.Ten));
			paraChuong.Append(runChuong);
			body.Append(paraChuong);

			foreach (var dieuluat in dieuluats.Where(dl => dl.Idchuong == chuong.Idchuong))
			{
                ParagraphProperties paraPropsDieuLuat = new ParagraphProperties(
					new Justification() { Val = JustificationValues.Both }  
					);
                Paragraph paraDieuLuat = new Paragraph(paraPropsDieuLuat);
				Run runDieuLuat = new Run(new RunProperties(new Bold(), new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
				runDieuLuat.Append(new Text(dieuluat.Noidung));
				paraDieuLuat.Append(runDieuLuat);
				body.Append(paraDieuLuat);

				foreach (var dieukhoan in dieukhoans.Where(dk => dk.Iddl == dieuluat.Iddl))
				{
					Paragraph paraDieuKhoan = new Paragraph(new ParagraphProperties(new Indentation() { Left = "720" }, new Justification() { Val = JustificationValues.Both }));
					Run runDieuKhoan = new Run(new RunProperties(new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
					runDieuKhoan.Append(new Text(dieukhoan.Noidung));
					paraDieuKhoan.Append(runDieuKhoan);
					body.Append(paraDieuKhoan);

					foreach (var dieukhoanthanhphan in dieukhoanthanhphans.Where(dktp => dktp.Iddk == dieukhoan.Iddk))
					{
						Paragraph paraDieuKhoanThanhPhan = new Paragraph(new ParagraphProperties(new Indentation() { Left = "1440" }, new Justification() { Val = JustificationValues.Both }));
						Run runDieuKhoanThanhPhan = new Run(new RunProperties(new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
						runDieuKhoanThanhPhan.Append(new Text(dieukhoanthanhphan.Noidung));
						paraDieuKhoanThanhPhan.Append(runDieuKhoanThanhPhan);
						body.Append(paraDieuKhoanThanhPhan);
					}
				}
			}
		}
		private void AddFooter(Body body, Vanban vanban, List<BosungVanban> noinhan)
		{

            // Tạo bảng để chứa Footer
            DocumentFormat.OpenXml.Wordprocessing.Table table = new DocumentFormat.OpenXml.Wordprocessing.Table();
            TableProperties tableProperties = new TableProperties(
                new TableWidth() { Width = "5000", Type = TableWidthUnitValues.Pct });
            table.AppendChild(tableProperties);

            // Tạo hàng mới của bảng
            TableRow tr = new TableRow();

            // Tạo ô thứ nhất của hàng và thêm nội dung
            TableCell tc1 = new TableCell();
            TableCellProperties tc1Properties = new TableCellProperties(new TableCellWidth() { Width = "2500", Type = TableWidthUnitValues.Pct });
            tc1.Append(tc1Properties);

            if (noinhan != null)
			{
                Paragraph paraNoinhan = new Paragraph(new ParagraphProperties(new Justification() { Val = JustificationValues.Left }));

                // Tạo run cho phần tử
                Run runNoinhan = new Run(new RunProperties(
                    new Bold(),
                    new FontSize() { Val = "26" },
                    new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" },
					new Text("Nơi nhận:")
                ));

                paraNoinhan.Append(runNoinhan);
                tc1.Append(paraNoinhan);

                for (int i = 0; i < noinhan.Count; i++)
                {
                    Paragraph paraHeaderItem = new Paragraph( new ParagraphProperties(new Justification() { Val = JustificationValues.Left }));

                    // Tạo run cho phần tử
                    Run runHeaderItem = new Run(new RunProperties(
                        new Italic(),
                        new FontSize() { Val = "26" },
                        new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }
                    ));

                    string content = noinhan[i].Noidung.Trim();
                    string separator = (i == noinhan.Count - 1) ? "." : ";";
                    runHeaderItem.Append(new Text(" - " + content + separator));

                    // Thêm run vào đoạn văn
                    paraHeaderItem.Append(runHeaderItem);

                    // Thêm đoạn văn vào ô
                    tc1.Append(paraHeaderItem);

                }
            }	

            tr.Append(tc1);

            // Tạo ô thứ hai của hàng và thêm tiêu đề
            TableCell tc2 = new TableCell(new TableCellProperties(new GridSpan() { Val = 3 }));
            TableCellProperties tc2Properties = new TableCellProperties(new TableCellWidth() { Width = "2500", Type = TableWidthUnitValues.Pct });
            tc2.Append(tc2Properties);
            Paragraph paraHeader2 = new Paragraph(new ParagraphProperties(new Justification() { Val = JustificationValues.Center }));

            // Dòng đầu tiên
            Run run1 = new Run(new RunProperties(new Bold(),new FontSize() { Val = "28" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
            run1.Append(new Text(vanban.ChucvuNguoikibanhanh));
            paraHeader2.Append(run1);

            // Xuống dòng
            paraHeader2.Append(new Break());

            // Dòng thứ hai
            Run run2 = new Run(new RunProperties(new FontSize() { Val = "26" }, new RunFonts() { Ascii = "Times New Roman", HighAnsi = "Times New Roman" }));
            Text text2 = new Text(vanban.Nguoikibanhanh);
            run2.Append(text2);
            paraHeader2.Append(run2);

            tc2.Append(paraHeader2);
            tr.Append(tc2);

            // Thêm hàng vào bảng
            table.Append(tr);

            // Thêm bảng vào body
            body.Append(table);

        }
        private void ConvertToWav(string inputFile, string outputFile)
        {
            using (var reader = new MediaFoundationReader(inputFile))
            using (var resampler = new WaveFormatConversionStream(new WaveFormat(16000, 16, 1), reader)) // 16,000 Hz, 16-bit, mono
            using (var writer = new WaveFileWriter(outputFile, resampler.WaveFormat))
            {
                resampler.CopyTo(writer);
            }
        }

        //speech to text
        [HttpGet]
        public IActionResult UploadAudio()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> UploadAudio(IFormFile audioFile)
        {
            if (audioFile == null || audioFile.Length == 0)
            {
                return BadRequest(new { message = "Cần có tệp âm thanh." });
            }

            try
            {
                // Logic lưu tệp và chuyển đổi âm thanh thành văn bản
                var uploadDirectory = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "uploads");
                if (!Directory.Exists(uploadDirectory))
                {
                    Directory.CreateDirectory(uploadDirectory);
                }

                var filePath = Path.Combine(uploadDirectory, audioFile.FileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    await audioFile.CopyToAsync(stream);
                }

                var wavFilePath = Path.Combine(uploadDirectory, Path.GetFileNameWithoutExtension(audioFile.FileName) + "audio_converted.wav");
                ConvertToWav(filePath, wavFilePath);


                var text = await _adminRepository.ConvertSpeechToTextAsync(wavFilePath);
                if (string.IsNullOrEmpty(text))
                {
                    return BadRequest(new { message = "Không nhận diện được nội dung. Hãy thử ghi âm lại với âm lượng rõ ràng hơn." });
                }
                return Ok(new { result = text });
            }
            catch (Exception ex)
            {
                // Trả về JSON báo lỗi chi tiết
                return StatusCode(500, new { error = "Có lỗi xảy ra khi xử lý tệp âm thanh.", details = ex.ToString() });
            }
        }
    }
}
