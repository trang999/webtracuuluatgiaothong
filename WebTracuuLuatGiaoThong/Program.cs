﻿using Azure.Storage.Blobs;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using WebTracuuLuatGiaoThong.Models;
using WebTracuuLuatGiaoThong.Repository.Admin;
using WebTracuuLuatGiaoThong.Repository.User;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddDbContext<DbDacnContext>(options =>
	options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

builder.Services.AddControllersWithViews();
builder.Services.AddIdentity<ApplicationUser, IdentityRole>()
.AddDefaultTokenProviders()
.AddDefaultUI()
.AddEntityFrameworkStores<DbDacnContext>();
builder.Services.AddIdentityCore<IdentityUser>()
	.AddUserManager<UserManager<IdentityUser>>()
	.AddEntityFrameworkStores<DbDacnContext>()
	.AddDefaultTokenProviders();
builder.Services.AddSingleton(x =>
{
    var connectionString = builder.Configuration["AzureBlobStorage:ConnectionString"];
    return new BlobServiceClient(connectionString);
});
builder.Services.AddRazorPages();
builder.Services.AddScoped<IUserRepository, EFUserRepository>();
builder.Services.AddScoped<IAdminRepository, EFAdminRepository>();
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
	app.UseExceptionHandler("/Home/Error");
}
app.UseStaticFiles();

app.UseRouting();
app.MapRazorPages();
app.UseAuthorization();

app.MapControllerRoute(
	name: "default",
	pattern: "{controller=User}/{action=Index}/{id?}");

app.Run();
