﻿using Microsoft.AspNetCore.Identity;
using Microsoft.CognitiveServices.Speech.Audio;
using Microsoft.CognitiveServices.Speech;
using Microsoft.EntityFrameworkCore;
using System;
using System.Drawing;
using WebTracuuLuatGiaoThong.Models;

namespace WebTracuuLuatGiaoThong.Repository.User
{
	public class EFUserRepository : IUserRepository
	{
		private readonly DbDacnContext _context;
		private readonly UserManager<ApplicationUser> _userManager;

        private readonly string _apiKey;
        private readonly string _region;
        public EFUserRepository(DbDacnContext context, UserManager<ApplicationUser> userManager, IConfiguration configuration)
		{
			_context = context;
			_userManager = userManager;
            _apiKey = configuration["AzureSpeech:ApiKey"];
            _region = configuration["AzureSpeech:Region"];
        }
		public async Task<IEnumerable<Vanban>> GetVanBanMoiAsync()//xuất toàn bộ văn bản mới (cách 1 năm)
		{
			DateOnly oneYearAgo = DateOnly.FromDateTime(DateTime.Today.AddYears(-1));
			DateOnly today = DateOnly.FromDateTime(DateTime.Today);

			return await _context.Vanbans.Where(p => p.Ngaybanhanh >= oneYearAgo).ToListAsync();
		}
		public async Task<IEnumerable<Tintuc>> GetTintucMoiAsync()//lấy tin tức mới (cách nữa năm)
		{
			return await _context.Tintucs.Where(p => p.Ngaycapnhat.Date <= DateTime.Today.Date && p.Trangthai == true).ToListAsync();
		}
		public async Task<IEnumerable<Vanban>> GetAllVanBanLuatAsync()//xuất toàn bộ văn bản 
		{
			var idloai = _context.LoaiVanBans.Where(p => p.TenLoaivb.ToLower() == "luật").Select(p => p.Idloaivb).FirstOrDefault();
			return await _context.Vanbans.Where(p => p.IdloaiVanBan == idloai).ToListAsync();
		}
		public async Task<IEnumerable<LoaiVanBan>> GetAllLoaiVbAsync() //xuất all loại văn bản để làm bộ lọc
		{
			return await _context.LoaiVanBans.ToListAsync();
		}
		public async Task<IEnumerable<Vanban>> GetVanBanByIDloaiAsync(int id) //XUẤT TOÀN BỘ TÊN VĂN BẢN THUỘC loại văn bản được chọn theo id
		{
			return await _context.Vanbans.Where(p => p.IdloaiVanBan == id).ToListAsync();
		}
		public async Task<List<Chuong>> GetChuongbyIDVanban(int id) //lấy toàn bộ nội dung chương của văn bản được chọn
		{
			return await _context.Chuongs.Where(p => p.Idvb == id).ToListAsync();
		}
		public async Task<(Vanban, List<Chuong>, List<DieuLuat>, List<DieuKhoan>, List<DieuKhoanThanhPhan>, List<BosungVanban>)> GetNoidungcuaVanban(int id) //lấy danh sách điều luật theo văn bản được chọn bằng id của văn bản
		{
			var vanban = await _context.Vanbans.FirstOrDefaultAsync(p => p.Idvb == id);
			List<Chuong> ListChuong = await GetChuongbyIDVanban(id);
			List<DieuLuat> Listdieuluat = new List<DieuLuat>();
			foreach (var item in ListChuong)
			{
				var dieuluat = await _context.DieuLuats.Where(p => p.Idchuong == item.Idchuong).ToListAsync();
				foreach (var item1 in dieuluat)
				{
					Listdieuluat.Add(item1);
				}
			}
			List<DieuKhoan> Listdieukhoan = new List<DieuKhoan>(); //list điều khoản trong điều luật
			foreach (var item in Listdieuluat)
			{
				var dieukhoan = await _context.DieuKhoans.Where(p => p.Iddl == item.Iddl).ToListAsync();
				foreach (var item1 in dieukhoan)
				{
					Listdieukhoan.Add(item1);
				}
			}
			List<DieuKhoanThanhPhan> Listdieukhoanthanhphan = new List<DieuKhoanThanhPhan>(); //list điều khoản thành phần trong điều khoản
			foreach (var item in Listdieukhoan)
			{
				var dieukhoanthanhphan = await _context.DieuKhoanThanhPhans.Where(p => p.Iddk == item.Iddk).ToListAsync();
				foreach (var item1 in dieukhoanthanhphan)
				{
					Listdieukhoanthanhphan.Add(item1);
				}
			}
			List<BosungVanban> Listbosung = new List<BosungVanban>();
			var Bosungs = await _context.BosungVanbans.Where(p => p.Idvb == id).ToListAsync();
			foreach (var item in Bosungs)
			{
				Listbosung.Add(item);
			}
			return (vanban, ListChuong, Listdieuluat, Listdieukhoan, Listdieukhoanthanhphan,Listbosung);
		}
		public async Task<IEnumerable<Tintuc>> GetallTintuc()//danh sách tin tức
		{
			return await _context.Tintucs.ToListAsync();
		}
		public async Task<Tintuc> GetTintucByID(int id)//hiển thị thông tin tin tức được chọn
		{
			return await _context.Tintucs.FirstOrDefaultAsync(p => p.Idtintuc == id);
		}
		public async Task<bool> addNote(Ghichu ghichu)//add thêm ghi chú
		{
			if (ghichu == null)
			{
				return false;
			}
			_context.Ghichus.Add(ghichu);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> deleteNote(int id)//xóa ghi chú
		{
			var note = await _context.Ghichus.FirstOrDefaultAsync(p => p.Id == id);
			if (note != null)
			{
				_context.Remove(note);
				await _context.SaveChangesAsync();
				return true;
			}
			else
			{
				return false;
			}
		}
		public async Task<bool> updateNote(Ghichu ghichu)//chỉnh sửa ghi chú
		{
			if (ghichu != null)
			{
				_context.Update(ghichu);
				await _context.SaveChangesAsync();
				return true;
			}
			else
			{
				return false;
			}
		}
		public async Task<IEnumerable<Ghichu>> viewNote(string iduser)//xuất note theo id user
		{
			var note = await _context.Ghichus.Where(p => p.Iduser == iduser).ToListAsync();
			if (note != null)
			{
				return note;
			}
			else
			{
				return null;
			}
		}
		public async Task<Ghichu> viewNotebyID(int id)//xuất note theo id 
		{
			return await _context.Ghichus.FirstOrDefaultAsync(p => p.Id == id);
		}

		public async Task<(List<Tintuc>, List<Vanban>)> Find(string text)//tìm kiếm thông tin
		{
			var ListTintuc = await _context.Tintucs.Where(p => p.Ten.Contains(text) || p.Ten.ToLower() == text.ToLower() /*|| p.Sohieu.ToLower() == text.ToLower()*/ || p.Noidung.Contains(text)).ToListAsync();
			var ListVanban = await _context.Vanbans.Where(p => p.Ten.Contains(text) || p.Ten.ToLower() == text.ToLower() || p.Sohieuvanban.ToLower() == text.ToLower()).ToListAsync();
			return (ListTintuc, ListVanban);
		}
		public async Task<int> GetVanBanByIDAsync(int id, int i)//lấy ra id văn bản
		{
			if (i == 1)//điều luật
			{
				var dieuluat = await _context.DieuLuats.FirstOrDefaultAsync(p => p.Iddl == id);
				var chuong = await _context.Chuongs.FirstOrDefaultAsync(p => p.Idchuong == dieuluat.Idchuong);
				return chuong.Idvb;
			}
			if (i == 2)//điều khoản
			{
				var dieukhoan = await _context.DieuKhoans.FirstOrDefaultAsync(p => p.Iddk == id);
				var dieuluat = await _context.DieuLuats.FirstOrDefaultAsync(p => p.Iddl == dieukhoan.Iddl);
				var chuong = await _context.Chuongs.FirstOrDefaultAsync(p => p.Idchuong == dieuluat.Idchuong);
				return chuong.Idvb;
			}
			if (i == 3)//điều khoản thành phần
			{
				var dieukhoantp = await _context.DieuKhoanThanhPhans.FirstOrDefaultAsync(p => p.Iddktp == id);
				var dieukhoan = await _context.DieuKhoans.FirstOrDefaultAsync(p => p.Iddk == dieukhoantp.Iddk);
				var dieuluat = await _context.DieuLuats.FirstOrDefaultAsync(p => p.Iddl == dieukhoan.Iddl);
				var chuong = await _context.Chuongs.FirstOrDefaultAsync(p => p.Idchuong == dieuluat.Idchuong);
				return chuong.Idvb;
			}
			return 0;
		}
        public async Task<List<BinhLuan>> GetBinhluantintuc(int id)
        {
            return await _context.BinhLuans.Where(p => p.IdTintuc == id).ToListAsync();
        }

		public async Task<List<TuCam>> GetallTucam()
		{
			return await _context.TuCams.ToListAsync();
		}
		public async Task SaveBinhluan(BinhLuan binhLuan)
		{
			_context.BinhLuans.Add(binhLuan);
			await _context.SaveChangesAsync();
		}
		public async Task<LoaiVanBan> GetTenloai(int id)
		{
            return await _context.LoaiVanBans.FirstOrDefaultAsync(p=>p.Idloaivb == id);
        }
        public async Task<List<BinhLuan>> BinhluanKiemduyet()
        {
            return await _context.BinhLuans.Where(p => p.TrangThai == false).ToListAsync();
        }
        public async Task<string> ConvertSpeechToTextAsync(string audioFilePath)
        {
            try
            {
                var speechConfig = SpeechConfig.FromSubscription(_apiKey, _region);
                speechConfig.SpeechRecognitionLanguage = "vi-VN";
                speechConfig.OutputFormat = OutputFormat.Detailed;
                var audioConfig = AudioConfig.FromWavFileInput(audioFilePath);

                using var recognizer = new SpeechRecognizer(speechConfig, audioConfig);
                var result = await recognizer.RecognizeOnceAsync();

                if (result.Reason == ResultReason.RecognizedSpeech)
                {
                    return result.Text;
                }
                else
                {
                    return $"Error: {result.Reason} - {result}";
                }
            }
            catch (Exception ex)
            {
                return $"Exception: {ex.Message}\nStack Trace: {ex.StackTrace}";
            }
        }
    }
}
