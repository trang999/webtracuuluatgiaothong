﻿using WebTracuuLuatGiaoThong.Models;

namespace WebTracuuLuatGiaoThong.Repository.User
{
	public interface IUserRepository
	{
		Task<IEnumerable<Vanban>> GetAllVanBanLuatAsync();
		Task<IEnumerable<LoaiVanBan>> GetAllLoaiVbAsync();//chưa dùng
		Task<IEnumerable<Vanban>> GetVanBanByIDloaiAsync(int id);//chưa dùng
		Task<(Vanban, List<Chuong>, List<DieuLuat>, List<DieuKhoan>, List<DieuKhoanThanhPhan>, List<BosungVanban>)> GetNoidungcuaVanban(int id);
		Task<IEnumerable<Tintuc>> GetallTintuc();
		Task<Tintuc> GetTintucByID(int id);
		Task<bool> addNote(Ghichu ghichu);
		Task<bool> deleteNote(int id);
		Task<bool> updateNote(Ghichu ghichu);
		Task<IEnumerable<Ghichu>> viewNote(string iduser);
		Task<Ghichu> viewNotebyID(int id);
		Task<IEnumerable<Vanban>> GetVanBanMoiAsync();
		Task<IEnumerable<Tintuc>> GetTintucMoiAsync();
		Task<(List<Tintuc>, List<Vanban>)> Find(string text);
		Task<int> GetVanBanByIDAsync(int id, int i);
		Task<List<Chuong>> GetChuongbyIDVanban(int id);
		Task<List<BinhLuan>> GetBinhluantintuc(int id);
		Task<List<TuCam>> GetallTucam();
		Task SaveBinhluan(BinhLuan binhLuan);
		Task<LoaiVanBan> GetTenloai(int id);
		Task<List<BinhLuan>> BinhluanKiemduyet();
        Task<string> ConvertSpeechToTextAsync(string audioFilePath);

	}
}
