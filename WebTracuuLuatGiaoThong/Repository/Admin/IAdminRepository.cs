﻿using WebTracuuLuatGiaoThong.Models;

namespace WebTracuuLuatGiaoThong.Repository.Admin
{
	public interface IAdminRepository
	{
		Task<bool> ChinhsuaChuong(Chuong chuong);
		Task<bool> ChinhsuaDieuluat(DieuLuat dieuLuat);
		Task<bool> ChinhsuaDieukhoan(DieuKhoan dieuLuat);
		Task<bool> ChinhsuaDieukhoanthanhphan(DieuKhoanThanhPhan dieuLuat);
		Task<Chuong> timkiemchuong(int id);
		Task<DieuLuat> timkiemdieuluat(int id);
		Task<DieuKhoan> timkiemdieukhoan(int id);
		Task<DieuKhoanThanhPhan> timkiemdieukhoanthanhphan(int id);
		Task<bool> ThemVanban(Vanban vanban);
		Task<bool> ThemChuong(Chuong chuong);
		Task<bool> ThemDieuLuat(DieuLuat dieuluat);
		Task<bool> ThemDieuKhoan(DieuKhoan dieukhoan);
		Task<bool> ThemDieuKhoanThanhPhan(DieuKhoanThanhPhan dieukhoantp);
		Task<List<LoaiVanBan>> ListLoaiVanban();
		Task<(Vanban, List<Chuong>, List<DieuLuat>, List<DieuKhoan>, List<DieuKhoanThanhPhan>, List<BosungVanban>)> GetNoidungcuaVanban(int id);
		Task<int> GetIDnewVanban();
		Task<bool> AddNewtintuc(Tintuc tintuc);
		Task<List<Tintuc>> TintucKiemduyet();
		Task<List<BinhLuan>> BinhluanKiemduyet();
		Task Xoabinhluan(List<BinhLuan> list);
		Task DuyetTintuc(Tintuc tintuc);
		Task<Tintuc> Timkiemtintuc(int id);
		Task<IEnumerable<Vanban>> GetallVanban();
		Task<Vanban> TimkiemVanban(int id);
		Task DeleteVanban(int id);
		Task<IEnumerable<Tintuc>> GetAllTintuc();
		Task DeleteTintuc(int id);
		Task<IEnumerable<TuCam>> GetallTucam();
		Task<bool> themTucam(TuCam tucam);
		Task XoaTucam(TuCam tucam); 
		Task<TuCam> timkiemTucam(int id);
		Task DeleteUserConnect(string idUser);
		Task<string> ConvertSpeechToTextAsync(string audioFilePath);
        Task<string> ExtractTextFromFileAsync(string filePath);
		Task<string> ExtractTextFromImageOCRSpaceAsync(string filePath);
		Task<bool> ThemBosung(BosungVanban bosung);
	}
}
