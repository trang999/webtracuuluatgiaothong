﻿using Microsoft.AspNetCore.Identity;
using Microsoft.CognitiveServices.Speech;
using Microsoft.EntityFrameworkCore;
using System.Drawing;
using System.Linq;
using WebTracuuLuatGiaoThong.Models;
using WebTracuuLuatGiaoThong.Repository.User;
using Microsoft.CognitiveServices.Speech;
using Microsoft.Extensions.Options;
using Microsoft.CognitiveServices.Speech.Audio;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using System.Text;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Newtonsoft.Json;
using Microsoft.Graph;
using Microsoft.Identity.Client;
using System;
using System.Net.Http.Headers;
using Azure.Identity;
using Microsoft.Graph.Models;

namespace WebTracuuLuatGiaoThong.Repository.Admin
{
    public class EFAdminRepository : IAdminRepository
    {
		private readonly DbDacnContext _context;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IUserRepository _userRepository;


        private readonly string _apiKey;
		private readonly string _region;

        private readonly string _apiKeyCV;
        private readonly string _endpointCV;

        private readonly string _apiKeyOCRSpace;
        private readonly string _urlOCRSpace;

        private readonly ComputerVisionClient _client;

        public EFAdminRepository(DbDacnContext context, UserManager<ApplicationUser> userManager, IUserRepository userRepository, IConfiguration configuration)
		{
			_context = context;
			_userManager = userManager;
			_userRepository = userRepository;
			_apiKey = configuration["AzureSpeech:ApiKey"];
			_region = configuration["AzureSpeech:Region"];
            _apiKeyCV = configuration["AzureComputerVision:ApiKey"];
            _endpointCV = configuration["AzureComputerVision:Endpoint"];
            _client = new ComputerVisionClient(new ApiKeyServiceClientCredentials(_apiKeyCV))
            {
                Endpoint = _endpointCV
            };
			_apiKeyOCRSpace = configuration["OCRSpaceApiClient:ApiKey"];
			_urlOCRSpace = configuration["OCRSpaceApiClient:Url"];
        }

        public async Task<bool> ChinhsuaChuong(Chuong chuong)
		{
			_context.Update(chuong);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> ChinhsuaDieuluat(DieuLuat dieuLuat)
		{
			_context.Update(dieuLuat);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> ChinhsuaDieukhoan(DieuKhoan dieuLuat)
		{
			_context.Update(dieuLuat);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> ChinhsuaDieukhoanthanhphan(DieuKhoanThanhPhan dieuLuat)
		{
			_context.Update(dieuLuat);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<Chuong> timkiemchuong(int id)
		{
			return await _context.Chuongs.FirstOrDefaultAsync(p => p.Idchuong == id);
		}
		public async Task<DieuLuat> timkiemdieuluat(int id)
		{
			return await _context.DieuLuats.FirstOrDefaultAsync(p => p.Iddl == id);
		}
		public async Task<DieuKhoan> timkiemdieukhoan(int id)
		{
			return await _context.DieuKhoans.FirstOrDefaultAsync(p => p.Iddk == id);
		}
		public async Task<DieuKhoanThanhPhan> timkiemdieukhoanthanhphan(int id)
		{
			return await _context.DieuKhoanThanhPhans.FirstOrDefaultAsync(p => p.Iddktp == id);
		}
		public async Task<bool> ThemVanban(Vanban vanban)
		{
			_context.Vanbans.Add(vanban);
			await _context.SaveChangesAsync();
			return true;
		}

        public async Task<bool> ThemBosung(BosungVanban bosung) 
        {
            _context.BosungVanbans.Add(bosung);
            await _context.SaveChangesAsync();
            return true;
        }
		public async Task<bool> ThemChuong(Chuong chuong)
		{
			_context.Chuongs.Add(chuong);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> ThemDieuLuat(DieuLuat dieuluat)
		{
			_context.DieuLuats.Add(dieuluat);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> ThemDieuKhoan(DieuKhoan dieukhoan)
		{

			_context.DieuKhoans.Add(dieukhoan);

			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<bool> ThemDieuKhoanThanhPhan(DieuKhoanThanhPhan dieukhoantp)
		{

			_context.DieuKhoanThanhPhans.Add(dieukhoantp);

			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<List<LoaiVanBan>> ListLoaiVanban()
		{
			return await _context.LoaiVanBans.ToListAsync();
		}
		public async Task<(Vanban, List<Chuong>, List<DieuLuat>, List<DieuKhoan>, List<DieuKhoanThanhPhan>, List<BosungVanban>)> GetNoidungcuaVanban(int id) //lấy danh sách điều luật theo văn bản được chọn bằng id của văn bản
		{
			var vanban = await _context.Vanbans.FirstOrDefaultAsync(p => p.Idvb == id);
			List<Chuong> ListChuong = await _userRepository.GetChuongbyIDVanban(id);
			List<DieuLuat> Listdieuluat = new List<DieuLuat>();
			foreach (var item in ListChuong)
			{
				var dieuluat = await _context.DieuLuats.Where(p => p.Idchuong == item.Idchuong).ToListAsync();
				foreach (var item1 in dieuluat)
				{
					Listdieuluat.Add(item1);
				}
			}
			List<DieuKhoan> Listdieukhoan = new List<DieuKhoan>(); //list điều khoản trong điều luật
			foreach (var item in Listdieuluat)
			{
				var dieukhoan = await _context.DieuKhoans.Where(p => p.Iddl == item.Iddl).ToListAsync();
				foreach (var item1 in dieukhoan)
				{
					Listdieukhoan.Add(item1);
				}
			}
			List<DieuKhoanThanhPhan> Listdieukhoanthanhphan = new List<DieuKhoanThanhPhan>(); //list điều khoản thành phần trong điều khoản
			foreach (var item in Listdieukhoan)
			{
				var dieukhoanthanhphan = await _context.DieuKhoanThanhPhans.Where(p => p.Iddk == item.Iddk).ToListAsync();
				foreach (var item1 in dieukhoanthanhphan)
				{
					Listdieukhoanthanhphan.Add(item1);
				}
			}
			List<BosungVanban> Listbosung = new List<BosungVanban>();
			var Bosungs = await _context.BosungVanbans.Where(p=>p.Idvb == id).ToListAsync();
			foreach (var item in Bosungs)
			{
				Listbosung.Add(item);
			}	
			return (vanban, ListChuong, Listdieuluat, Listdieukhoan, Listdieukhoanthanhphan, Listbosung);
		}
		public async Task<int> GetIDnewVanban()
		{
			Vanban vb = await _context.Vanbans.OrderByDescending(v => v.Idvb).FirstOrDefaultAsync();
			return vb.Idvb;
		}
		public async Task<bool> AddNewtintuc(Tintuc tintuc)
		{
			_context.Tintucs.Add(tintuc);
			await _context.SaveChangesAsync();
			return true;
		}
		public async Task<List<Tintuc>> TintucKiemduyet()
		{
			return await _context.Tintucs.Where(p => p.Trangthai == false).ToListAsync();
		}
		public async Task<List<BinhLuan>> BinhluanKiemduyet()
		{
			return await _context.BinhLuans.Where(p => p.TrangThai == false).ToListAsync();
		}
		public async Task Xoabinhluan(List<BinhLuan> list) // xóa để gỡ lệnh cấm bình luận
		{
			foreach(var item in  list)
			{
				_context.Remove(item);
			}	
			await _context.SaveChangesAsync();
		}

		public async Task DuyetTintuc(Tintuc tintuc) // Duyệt toàn bộ tin tức đang chờ duyệt 
		{
			_context.Tintucs.Update(tintuc);
			await _context.SaveChangesAsync();
		}

		public async Task<Tintuc> Timkiemtintuc(int id) // tìm kiếm tin tức theo id
		{
			return await _context.Tintucs.FirstOrDefaultAsync(p=>p.Idtintuc == id);
		}
        public async Task<IEnumerable<Vanban>> GetallVanban()//xuất toàn bộ văn bản mới
        {
            return await _context.Vanbans.ToListAsync();
        }
		public async Task<Vanban> TimkiemVanban(int id)
		{
			return await _context.Vanbans.FirstOrDefaultAsync(p=>p.Idvb == id);
		}
		public async Task DeleteVanban(int id)
		{
			var vanban = _context.Vanbans.FirstOrDefault(p=>p.Idvb == id);
			var chuongs = await _context.Chuongs.Where(p => p.Idvb == id).ToListAsync();

			var chuongIds = chuongs.Select(c => c.Idchuong).ToList();
			var dieus = await _context.DieuLuats.Where(d => chuongIds.Contains(d.Idchuong)).ToListAsync();

			var dieuluatIds = dieus.Select(c=>c.Iddl).ToList();
			var dieukhoans = await _context.DieuKhoans.Where(d => dieuluatIds.Contains(d.Iddl)).ToListAsync();

			var dieukhoanIds = dieukhoans.Select(c => c.Iddk).ToList();
			var dieukhoantps = await _context.DieuKhoanThanhPhans.Where(d => dieukhoanIds.Contains(d.Iddk)).ToListAsync();

			var dieukhoantpIds = dieukhoantps.Select(c => c.Iddktp).ToList();
			var ghichus = await _context.Ghichus
									.Where(g =>
										(g.Iddl != null && dieuluatIds.Contains(g.Iddl.Value)) ||
										(g.Iddk != null && dieukhoanIds.Contains(g.Iddk.Value)) ||
										(g.Iddktp != null && dieukhoantpIds.Contains(g.Iddktp.Value)))
									.ToListAsync();


			_context.DieuKhoanThanhPhans.RemoveRange(dieukhoantps);
			_context.DieuKhoans.RemoveRange(dieukhoans);
			_context.DieuLuats.RemoveRange(dieus);
			_context.Chuongs.RemoveRange(chuongs);
			_context.Ghichus.RemoveRange(ghichus);	
			_context.Vanbans.Remove(vanban);

			_context.SaveChanges();	
		}

		public async Task<IEnumerable<Tintuc>> GetAllTintuc()//lấy tin tức mới
		{
			return await _context.Tintucs.ToListAsync();
		}
		public async Task DeleteTintuc(int id)
		{
			var tintuc = await _context.Tintucs.FirstOrDefaultAsync(p=>p.Idtintuc == id);
			var binhluans = await _context.BinhLuans.Where(p => p.IdTintuc == tintuc.Idtintuc).ToListAsync();
			_context.BinhLuans.RemoveRange(binhluans);
			_context.Tintucs.Remove(tintuc);

			_context.SaveChanges();	

		}
		public async Task<IEnumerable<TuCam>> GetallTucam()
		{
			return await _context.TuCams.ToListAsync();
		}
		public async Task<bool> themTucam(TuCam tucam)
		{
			// Lấy danh sách tucam một cách bất đồng bộ
			List<TuCam> Tucams = await GetallTucam() as List<TuCam>;

			// Kiểm tra xem tucam đã tồn tại chưa (có thể dùng thuộc tính như 'Ten' nếu có)
			if (Tucams.Any(t => t.Noidung == tucam.Noidung)) // Thay đổi 'Ten' bằng thuộc tính thực tế để kiểm tra
			{
				return false;
			}
			else
			{
				_context.TuCams.Add(tucam);
				await _context.SaveChangesAsync();
				return true;
			}
			
		}
		public async Task<TuCam> timkiemTucam(int id)
		{
			return await _context.TuCams.FirstOrDefaultAsync(p=>p.Id == id);
		}
		public async Task XoaTucam(TuCam tucam)
		{
			_context.TuCams.Remove(tucam);
			await _context.SaveChangesAsync();	
		}
        public async Task DeleteUserConnect(string idUser)
        {
            var Binhluans = await _context.BinhLuans.FirstOrDefaultAsync(p=>p.IdUser == idUser);
			var Ghichus = await _context.Ghichus.FirstOrDefaultAsync(p=>p.Iduser == idUser);

            if (Binhluans != null)
            {
                _context.BinhLuans.RemoveRange(Binhluans);
            }

            if (Ghichus != null)
            {
                _context.Ghichus.RemoveRange(Ghichus);
            }

            await _context.SaveChangesAsync();
        }
		public async Task<string> ConvertSpeechToTextAsync(string audioFilePath)
		{
            try
            {
                var speechConfig = SpeechConfig.FromSubscription(_apiKey, _region);
                speechConfig.SpeechRecognitionLanguage = "vi-VN";
                speechConfig.OutputFormat = OutputFormat.Detailed;
                var audioConfig = AudioConfig.FromWavFileInput(audioFilePath);

                using var recognizer = new SpeechRecognizer(speechConfig, audioConfig);
                var result = await recognizer.RecognizeOnceAsync();

                if (result.Reason == ResultReason.RecognizedSpeech)
                {
                    return result.Text;
                }
                else
                {
                    return $"Error: {result.Reason} - {result}";
                }
            }
            catch (Exception ex)
            {
                return $"Exception: {ex.Message}\nStack Trace: {ex.StackTrace}";
            }
        }

		public async Task<string> ExtractTextFromFileAsync(string filePath)
		{
			var textResult = new StringBuilder();

			try
			{
				using var imageStream = File.OpenRead(filePath);
				var readResults = await _client.ReadInStreamAsync(imageStream);
				string operationId = readResults.OperationLocation.Split('/').Last();

				ReadOperationResult results;
				do
				{
					await Task.Delay(1000);
					results = await _client.GetReadResultAsync(Guid.Parse(operationId));
				}
				while (results.Status == OperationStatusCodes.Running);

				if (results.Status == OperationStatusCodes.Succeeded)
				{
					foreach (var page in results.AnalyzeResult.ReadResults)
					{
						foreach (var line in page.Lines)
						{
							textResult.AppendLine(line.Text);
						}
					}
				}
			}
			catch (Exception ex)
			{
				// Ghi log chi tiết lỗi xảy ra trong quá trình gọi API của Azure
				Console.WriteLine($"Error during text extraction: {ex.Message}\nStack Trace: {ex.StackTrace}");
				throw; // Hoặc có thể trả về thông báo lỗi cho client
			}

			return textResult.ToString();
		}
        public async Task<string> ExtractTextFromImageOCRSpaceAsync(string filePath)
        {
            using var client = new HttpClient();
            using var content = new MultipartFormDataContent();

            // Thêm API Key và ảnh vào yêu cầu
            content.Add(new StringContent(_apiKeyOCRSpace), "apikey");
            content.Add(new StreamContent(File.OpenRead(filePath)), "file", Path.GetFileName(filePath));

            // Đảm bảo tham số ngôn ngữ là "vie" để nhận diện tiếng Việt
            /*content.Add(new StringContent("vie"), "language");*/

            try
            {
                // Gửi yêu cầu POST đến API OCR.Space
                var response = await client.PostAsync(_urlOCRSpace, content);

                if (response.IsSuccessStatusCode)
                {
                    var jsonResponse = await response.Content.ReadAsStringAsync();
                    var ocrResult = JsonConvert.DeserializeObject<OCRResult>(jsonResponse);

                    // Kiểm tra và trả về văn bản nhận diện được
                    return ocrResult?.ParsedResults?[0]?.ParsedText ?? "Không nhận diện được văn bản";
                }
                else
                {
                    // Xử lý lỗi nếu yêu cầu không thành công
                    throw new Exception("Lỗi khi gọi OCR.Space API");
                }
            }
            catch (Exception ex)
            {
                // Xử lý lỗi khi gửi yêu cầu
                throw new Exception($"Lỗi khi xử lý ảnh: {ex.Message}");
            }
        }
        // Lớp để phân tích kết quả trả về từ OCR.Space API
        public class OCRResult
        {
            public ParsedResult[] ParsedResults { get; set; }
        }

        public class ParsedResult
        {
            public string ParsedText { get; set; }
        }



        //api 
        
    }
}