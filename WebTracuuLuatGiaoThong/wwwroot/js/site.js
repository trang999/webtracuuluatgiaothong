﻿// Phân trang tin tức
document.addEventListener("DOMContentLoaded", function () {
    // Lấy tất cả các tin tức
    var newsArticles = document.querySelectorAll('.box');
    // Số lượng tin tức mỗi trang
    var itemsPerPage = 5;
    // Tính số lượng trang
    var totalPages = Math.ceil(newsArticles.length / itemsPerPage);
    // Hiển thị trang đầu tiên khi tải trang
    displayNews(0);

    // Hàm hiển thị tin tức theo trang
    function displayNews(pageIndex) {
        var startIndex = pageIndex * itemsPerPage; // Bắt đầu từ tin tức thứ mấy
        var endIndex = Math.min(startIndex + itemsPerPage, newsArticles.length); // Kết thúc ở tin tức thứ mấy
        // Ẩn tất cả các tin tức
        newsArticles.forEach(function (article, index) {
            if (index >= startIndex && index < endIndex) {
                article.style.display = 'block'; // Hiển thị tin tức nếu nằm trong phạm vi trang hiện tại
            } else {
                article.style.display = 'none'; // Ẩn tin tức nếu không nằm trong phạm vi trang hiện tại
            }
        });
        // Hiển thị các nút phân trang
        displayPagination(pageIndex);
        // Cuộn đến phần tin nổi bật
        var highlightNews = document.querySelector('.highlight-news');
        if (highlightNews) {
            highlightNews.scrollIntoView({ behavior: 'smooth', block: 'start' });
        }
    }

    // Hàm tạo và hiển thị nút phân trang
    function displayPagination(activePage) {
        var paginationDiv = document.querySelector('.pagination');
        paginationDiv.innerHTML = ''; // Xóa nút phân trang cũ
        for (var i = 0; i < totalPages; i++) {
            var pageButton = document.createElement('button');
            pageButton.innerText = i + 1;
            pageButton.setAttribute('data-page', i);
            if (i === activePage) {
                pageButton.classList.add('active');
            }
            pageButton.addEventListener('click', function () {
                var pageIndex = parseInt(this.getAttribute('data-page'));
                displayNews(pageIndex);
            });
            paginationDiv.appendChild(pageButton);
        }
    }
});
//Phân trang văn bản mới