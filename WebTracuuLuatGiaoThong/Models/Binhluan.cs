﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class BinhLuan
{
    public int Id { get; set; }

    public string IdUser { get; set; } = null!;

    public int IdTintuc { get; set; }

    public int? IdPhanhoi { get; set; }

    public DateTime Ngaybinhluan { get; set; }

    public string Noidung { get; set; } = null!;

    public bool TrangThai { get; set; }

    public virtual Tintuc IdTintucNavigation { get; set; } = null!;

    public virtual AspNetUser IdUserNavigation { get; set; } = null!;
}
