﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class BosungVanban
{
    public int Id { get; set; }

    public int Idvb { get; set; }

    public int IdloaiBs { get; set; }

    public string Noidung { get; set; } = null!;

    public virtual LoaiBosung IdloaiBsNavigation { get; set; } = null!;

    public virtual Vanban IdvbNavigation { get; set; } = null!;
}
