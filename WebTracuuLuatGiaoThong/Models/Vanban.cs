﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class Vanban
{
    public int Idvb { get; set; }

    public int IdloaiVanBan { get; set; }

    public string Ten { get; set; } = null!;

    public string Sohieuvanban { get; set; } = null!;

    public DateOnly Ngaybanhanh { get; set; }

    public string Coquanphathanh { get; set; } = null!;

    public DateOnly Ngaycapnhat { get; set; }

    public string ChucvuNguoikibanhanh { get; set; } = null!;

    public string Nguoikibanhanh { get; set; } = null!;

    public string? Noibanhanh { get; set; }

    public virtual ICollection<BosungVanban> BosungVanbans { get; set; } = new List<BosungVanban>();

    public virtual ICollection<Chuong> Chuongs { get; set; } = new List<Chuong>();

    public virtual LoaiVanBan IdloaiVanBanNavigation { get; set; } = null!;
}
