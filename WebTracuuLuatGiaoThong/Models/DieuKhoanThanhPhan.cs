﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class DieuKhoanThanhPhan
{
    public int Iddktp { get; set; }

    public int Iddk { get; set; }

    public string Noidung { get; set; } = null!;

    public virtual ICollection<Ghichu> Ghichus { get; set; } = new List<Ghichu>();

    public virtual DieuKhoan IddkNavigation { get; set; } = null!;
}
