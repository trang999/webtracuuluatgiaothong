﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class Ghichu
{
    public int Id { get; set; }

    public string Iduser { get; set; } = null!;

    public int? Iddl { get; set; }

    public int? Iddk { get; set; }

    public int? Iddktp { get; set; }

    public string Noidung { get; set; } = null!;

    public virtual DieuKhoan? IddkNavigation { get; set; }

    public virtual DieuKhoanThanhPhan? IddktpNavigation { get; set; }

    public virtual DieuLuat? IddlNavigation { get; set; }

    public virtual AspNetUser IduserNavigation { get; set; } = null!;
}
