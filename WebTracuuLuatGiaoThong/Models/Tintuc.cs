﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class Tintuc
{
    public int Idtintuc { get; set; }

    public string Iduser { get; set; } = null!;

    public string Ten { get; set; } = null!;

    public string Noidung { get; set; } = null!;

    public bool Trangthai { get; set; }

    public string? Tenhinhanh { get; set; }

    public DateTime Ngaycapnhat { get; set; }

    public virtual ICollection<BinhLuan> BinhLuans { get; set; } = new List<BinhLuan>();

    public virtual ICollection<HinhanhTintuc> HinhanhTintucs { get; set; } = new List<HinhanhTintuc>();

    public virtual AspNetUser IduserNavigation { get; set; } = null!;
}
