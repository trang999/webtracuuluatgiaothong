﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class LoaiVanBan
{
    public int Idloaivb { get; set; }

    public string TenLoaivb { get; set; } = null!;

    public virtual ICollection<Vanban> Vanbans { get; set; } = new List<Vanban>();
}
