﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class Chuong
{
    public int Idchuong { get; set; }

    public int Idvb { get; set; }

    public string Ten { get; set; } = null!;

    public virtual ICollection<DieuLuat> DieuLuats { get; set; } = new List<DieuLuat>();

    public virtual Vanban IdvbNavigation { get; set; } = null!;
}
