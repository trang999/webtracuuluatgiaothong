﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace WebTracuuLuatGiaoThong.Models
{
	public class ApplicationUser : IdentityUser
	{
		[Required]
		public string Address { get; set; }
		public string? PhoneNumber { get; set; }
	}
}
