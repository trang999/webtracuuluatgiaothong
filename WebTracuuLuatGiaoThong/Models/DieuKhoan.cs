﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class DieuKhoan
{
    public int Iddk { get; set; }

    public int Iddl { get; set; }

    public string Noidung { get; set; } = null!;

    public virtual ICollection<DieuKhoanThanhPhan> DieuKhoanThanhPhans { get; set; } = new List<DieuKhoanThanhPhan>();

    public virtual ICollection<Ghichu> Ghichus { get; set; } = new List<Ghichu>();

    public virtual DieuLuat IddlNavigation { get; set; } = null!;
}
