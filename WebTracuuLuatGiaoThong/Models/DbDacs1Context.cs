﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace WebTracuuLuatGiaoThong.Models;

public partial class DbDacnContext : IdentityDbContext<ApplicationUser>
{
    public DbDacnContext()
    {
    }

    public DbDacnContext(DbContextOptions<DbDacnContext> options)
        : base(options)
    {
    }
    public virtual DbSet<BinhLuan> BinhLuans { get; set; }

    public virtual DbSet<BosungVanban> BosungVanbans { get; set; }

    public virtual DbSet<Chuong> Chuongs { get; set; }

    public virtual DbSet<DieuKhoan> DieuKhoans { get; set; }

    public virtual DbSet<DieuKhoanThanhPhan> DieuKhoanThanhPhans { get; set; }

    public virtual DbSet<DieuLuat> DieuLuats { get; set; }

    public virtual DbSet<Ghichu> Ghichus { get; set; }

    public virtual DbSet<HinhanhTintuc> HinhanhTintucs { get; set; }

    public virtual DbSet<LoaiBosung> LoaiBosungs { get; set; }

    public virtual DbSet<LoaiVanBan> LoaiVanBans { get; set; }

    public virtual DbSet<Tintuc> Tintucs { get; set; }

    public virtual DbSet<TuCam> TuCams { get; set; }

    public virtual DbSet<Vanban> Vanbans { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see https://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=LAPTOP-V2T6O7O6\\TRANG;Initial Catalog=DbDACS_1;User ID=sa;Password=123;Connect Timeout=30;Encrypt=False;Trust Server Certificate=False;Application Intent=ReadWrite;Multi Subnet Failover=False");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
		base.OnModelCreating(modelBuilder);

		modelBuilder.Entity<IdentityUserLogin<string>>()
			.HasKey(login => new { login.LoginProvider, login.ProviderKey });

		modelBuilder.Entity<AspNetUserLogin>(entity =>
		{
			entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

			entity.HasIndex(e => e.UserId, "IX_AspNetUserLogins_UserId");

			entity.HasOne(d => d.User).WithMany(p => p.AspNetUserLogins).HasForeignKey(d => d.UserId);
		});
		modelBuilder.Entity<AspNetUserToken>(entity =>
		{
			entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

			entity.HasOne(d => d.User).WithMany(p => p.AspNetUserTokens).HasForeignKey(d => d.UserId);
		});


		modelBuilder.Entity<BinhLuan>(entity =>
        {
            entity.ToTable("BinhLuan");

            entity.Property(e => e.IdUser).HasMaxLength(450);
            entity.Property(e => e.Ngaybinhluan).HasColumnType("datetime");

            entity.HasOne(d => d.IdTintucNavigation).WithMany(p => p.BinhLuans)
                .HasForeignKey(d => d.IdTintuc)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_BinhLuan_Tintuc1");

            entity.HasOne(d => d.IdUserNavigation).WithMany(p => p.BinhLuans)
                .HasForeignKey(d => d.IdUser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_BinhLuan_AspNetUsers");
        });

        modelBuilder.Entity<BosungVanban>(entity =>
        {
            entity.ToTable("Bosung_Vanban");

            entity.Property(e => e.IdloaiBs).HasColumnName("IDLoaiBS");
            entity.Property(e => e.Idvb).HasColumnName("IDvb");
            entity.Property(e => e.Noidung).HasMaxLength(255);

            entity.HasOne(d => d.IdloaiBsNavigation).WithMany(p => p.BosungVanbans)
                .HasForeignKey(d => d.IdloaiBs)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Bosung_Vanban_LoaiBosung");

            entity.HasOne(d => d.IdvbNavigation).WithMany(p => p.BosungVanbans)
                .HasForeignKey(d => d.Idvb)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Bosung_Vanban_Vanban");
        });

        modelBuilder.Entity<Chuong>(entity =>
        {
            entity.HasKey(e => e.Idchuong);

            entity.ToTable("Chuong");

            entity.HasIndex(e => e.Idvb, "IX_Chuong_IDvb");

            entity.Property(e => e.Idchuong).HasColumnName("IDchuong");
            entity.Property(e => e.Idvb).HasColumnName("IDvb");

            entity.HasOne(d => d.IdvbNavigation).WithMany(p => p.Chuongs)
                .HasForeignKey(d => d.Idvb)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Chuong_Vanban");
        });

        modelBuilder.Entity<DieuKhoan>(entity =>
        {
            entity.HasKey(e => e.Iddk);

            entity.ToTable("DieuKhoan");

            entity.HasIndex(e => e.Iddl, "IX_DieuKhoan_IDdl");

            entity.Property(e => e.Iddk).HasColumnName("IDdk");
            entity.Property(e => e.Iddl).HasColumnName("IDdl");

            entity.HasOne(d => d.IddlNavigation).WithMany(p => p.DieuKhoans)
                .HasForeignKey(d => d.Iddl)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DieuKhoan_DieuLuat");
        });

        modelBuilder.Entity<DieuKhoanThanhPhan>(entity =>
        {
            entity.HasKey(e => e.Iddktp);

            entity.ToTable("DieuKhoanThanhPhan");

            entity.HasIndex(e => e.Iddk, "IX_DieuKhoanThanhPhan_IDdk");

            entity.Property(e => e.Iddktp).HasColumnName("IDdktp");
            entity.Property(e => e.Iddk).HasColumnName("IDdk");

            entity.HasOne(d => d.IddkNavigation).WithMany(p => p.DieuKhoanThanhPhans)
                .HasForeignKey(d => d.Iddk)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DieuKhoanThanhPhan_DieuKhoan");
        });

        modelBuilder.Entity<DieuLuat>(entity =>
        {
            entity.HasKey(e => e.Iddl);

            entity.ToTable("DieuLuat");

            entity.HasIndex(e => e.Idchuong, "IX_DieuLuat_IDchuong");

            entity.Property(e => e.Iddl).HasColumnName("IDdl");
            entity.Property(e => e.Idchuong).HasColumnName("IDchuong");

            entity.HasOne(d => d.IdchuongNavigation).WithMany(p => p.DieuLuats)
                .HasForeignKey(d => d.Idchuong)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_DieuLuat_Chuong");
        });

        modelBuilder.Entity<Ghichu>(entity =>
        {
            entity.ToTable("Ghichu");

            entity.HasIndex(e => e.Iddk, "IX_Ghichu_IDdk");

            entity.HasIndex(e => e.Iddktp, "IX_Ghichu_IDdktp");

            entity.HasIndex(e => e.Iddl, "IX_Ghichu_IDdl");

            entity.Property(e => e.Id).HasColumnName("ID");
            entity.Property(e => e.Iddk).HasColumnName("IDdk");
            entity.Property(e => e.Iddktp).HasColumnName("IDdktp");
            entity.Property(e => e.Iddl).HasColumnName("IDdl");
            entity.Property(e => e.Iduser)
                .HasMaxLength(450)
                .HasColumnName("IDUser");

            entity.HasOne(d => d.IddkNavigation).WithMany(p => p.Ghichus)
                .HasForeignKey(d => d.Iddk)
                .HasConstraintName("FK_Ghichu_DieuKhoan");

            entity.HasOne(d => d.IddktpNavigation).WithMany(p => p.Ghichus)
                .HasForeignKey(d => d.Iddktp)
                .HasConstraintName("FK_Ghichu_DieuKhoanThanhPhan");

            entity.HasOne(d => d.IddlNavigation).WithMany(p => p.Ghichus)
                .HasForeignKey(d => d.Iddl)
                .HasConstraintName("FK_Ghichu_DieuLuat");

            entity.HasOne(d => d.IduserNavigation).WithMany(p => p.Ghichus)
                .HasForeignKey(d => d.Iduser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Ghichu_AspNetUsers");
        });

        modelBuilder.Entity<HinhanhTintuc>(entity =>
        {
            entity.HasKey(e => e.Idhinhanh);

            entity.ToTable("HinhanhTintuc");

            entity.Property(e => e.Idhinhanh).HasColumnName("IDHinhanh");
            entity.Property(e => e.Idtintuc).HasColumnName("IDtintuc");

            entity.HasOne(d => d.IdtintucNavigation).WithMany(p => p.HinhanhTintucs)
                .HasForeignKey(d => d.Idtintuc)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_HinhanhTintuc_Tintuc");
        });

        modelBuilder.Entity<LoaiBosung>(entity =>
        {
            entity.HasKey(e => e.IdloaiBs);

            entity.ToTable("LoaiBosung");

            entity.Property(e => e.IdloaiBs).HasColumnName("IDLoaiBS");
            entity.Property(e => e.Tenloai)
                .HasMaxLength(10)
                .IsFixedLength();
        });

        modelBuilder.Entity<LoaiVanBan>(entity =>
        {
            entity.HasKey(e => e.Idloaivb);

            entity.ToTable("LoaiVanBan");

            entity.Property(e => e.Idloaivb).HasColumnName("IDLoaivb");
            entity.Property(e => e.TenLoaivb).HasMaxLength(200);
        });

        modelBuilder.Entity<Tintuc>(entity =>
        {
            entity.HasKey(e => e.Idtintuc);

            entity.ToTable("Tintuc");

            entity.Property(e => e.Idtintuc).HasColumnName("IDtintuc");
            entity.Property(e => e.Iduser)
                .HasMaxLength(450)
                .HasColumnName("IDUser");
            entity.Property(e => e.Ngaycapnhat).HasColumnType("datetime");

            entity.HasOne(d => d.IduserNavigation).WithMany(p => p.Tintucs)
                .HasForeignKey(d => d.Iduser)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Tintuc_AspNetUsers");
        });

        modelBuilder.Entity<TuCam>(entity =>
        {
            entity.ToTable("TuCam");

            entity.Property(e => e.Noidung).HasMaxLength(100);
        });

        modelBuilder.Entity<Vanban>(entity =>
        {
            entity.HasKey(e => e.Idvb);

            entity.ToTable("Vanban");

            entity.HasIndex(e => e.IdloaiVanBan, "IX_Vanban_IDLoaiVanBan");

            entity.Property(e => e.Idvb).HasColumnName("IDvb");
            entity.Property(e => e.ChucvuNguoikibanhanh).HasDefaultValue("");
            entity.Property(e => e.IdloaiVanBan).HasColumnName("IDLoaiVanBan");
            entity.Property(e => e.Nguoikibanhanh).HasDefaultValue("");
            entity.Property(e => e.Noibanhanh).HasMaxLength(50);
            entity.Property(e => e.Sohieuvanban).HasMaxLength(100);

            entity.HasOne(d => d.IdloaiVanBanNavigation).WithMany(p => p.Vanbans)
                .HasForeignKey(d => d.IdloaiVanBan)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK_Vanban_LoaiVanBan");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
