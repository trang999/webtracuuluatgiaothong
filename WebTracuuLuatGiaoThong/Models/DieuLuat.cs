﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class DieuLuat
{
    public int Iddl { get; set; }

    public int Idchuong { get; set; }

    public string Noidung { get; set; } = null!;

    public virtual ICollection<DieuKhoan> DieuKhoans { get; set; } = new List<DieuKhoan>();

    public virtual ICollection<Ghichu> Ghichus { get; set; } = new List<Ghichu>();

    public virtual Chuong IdchuongNavigation { get; set; } = null!;
}
