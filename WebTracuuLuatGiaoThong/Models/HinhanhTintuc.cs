﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class HinhanhTintuc
{
    public int Idhinhanh { get; set; }

    public int Idtintuc { get; set; }

    public virtual Tintuc IdtintucNavigation { get; set; } = null!;
}
