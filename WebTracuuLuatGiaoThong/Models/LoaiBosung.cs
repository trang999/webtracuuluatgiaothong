﻿using System;
using System.Collections.Generic;

namespace WebTracuuLuatGiaoThong.Models;

public partial class LoaiBosung
{
    public int IdloaiBs { get; set; }

    public string Tenloai { get; set; } = null!;

    public virtual ICollection<BosungVanban> BosungVanbans { get; set; } = new List<BosungVanban>();
}
